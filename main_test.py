from intent_classifier.Utils.load_parameters import *
from intent_classifier.Test.accuracy_test import *
from intent_classifier.Test.conflict_test import *

""" Execute from CODE"""
# ----------------------------------------------------------------------
params, model_config = load_parameters()
# ----------------------------------------------------------------------

if params.get('url_service') is not None:
    Accuracy_Test = AccuracyTest(params)
    Accuracy_Test.main()
elif params.get('url_service') is None:
    Conflict_test = ConflictTest(params)
    Conflict_test.main()

# """If you want to run from outside world, use the following snippet."""
# import pandas as pd
# import json
#
# # MANDATORY ARGUMENTS FOR BOTH TEST
# TEST_DATA = pd.read_csv("/home/juan/CIRA_ML_2/data/LPL_ISA_TEST_210212019.csv", encoding='utf-8')
# MODEL_DIR = "/home/juan/CIRA_ML_2/models/"
#
# # OPTIONAL ARGUMENTS
# ENABLE_MULTITHREADING = False
#
# # ARGUMENTS FOR ACCURACY TEST ONLY
# URL_SERVICE = 'http://juan.localhost.run'
# LANG = None
# TOPICS_DATA_DF = None
# SYNONYMS_JSON = '/home/juan/CIRA_ML_2/data/synonyms_5c6bb06e6994c8001a1487c8_0221.json'

# ARGUMENTS FOR CONFLICT TEST ONLY
# URL_SERVICE = None
# LANG = en
# TOPICS_DATA_DF = pd.read_csv('/home/juan/CIRA_ML_2/data/LPL_ISA_TOPICS_18012019.csv', encoding='utf-8')
# SYNONYMS_JSON = '/home/juan/CIRA_ML_2/data/synonyms_5c382b81b715cd0015e6795f_0118.json'

# if SYNONYMS_JSON is not None:
#     with open(SYNONYMS_JSON) as f:
#         SYNONYMS_JSON = json.load(f)
# else:
#     pass
#
# params, model_config = load_parameters(test_data=TEST_DATA, model_dir=MODEL_DIR, multithreading=ENABLE_MULTITHREADING,
#                                        topics_csv=TOPICS_DATA_DF, lang=LANG, synonyms_json=SYNONYMS_JSON,
#                                        url_service=URL_SERVICE)
#
# if params.get('test_data') is not None and params.get('url_service') is None:
#     Conflict_test = ConflictTest(params)
#     results_df = Conflict_test.main()
# elif params.get('test_data') is not None and params.get('url_service') is not None:
#     Accuracy_Test = AccuracyTest(params)
#     Accuracy_Test.main()
# else:
#     pass
