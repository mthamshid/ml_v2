# python setup.py sdist bdist_wheel
from setuptools import setup, find_packages

setup(name='ml_v2',
      version='2.0',
      description='An Intent classifier with multi-language support.',
      long_description='This is the PIP package of intent classifier with multi-language built using keras on TF. ',
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Programming Language :: Python :: 3',
          'Topic :: Text classification:: Linguistic',
      ],
      keywords='ml module intent classifier',
      url='https://rejin_jose@bitbucket.org/alejandrocognicor/ml_v2.git',
      author='CogniCor',
      author_email='mlteam@cognicor.com',

      packages=['Predict', 'Utils', 'Train', 'Test'],
      install_requires=['markdown', 'Flask==1.0.2', 'gunicorn==19.8.1', 'urllib3==1.22', 'h5py==2.7.1', 'nltk==3.2.5',
                        'textblob==0.15.1', 'textblob-de==0.4.2', 'numpy', 'pandas==0.22.0', 'requests==2.18.4',
                        'scikit-learn==0.19.1', 'scipy==1.1.0', 'gensim==3.4.0', 'tensorflow==1.8.0', 'Keras==2.1.6',
                        'spacy', 'mpu==0.9.1', 'langdetect==1.0.7', 'hunspell'],
      include_package_data=True,
      zip_safe=False)
