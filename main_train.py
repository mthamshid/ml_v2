from intent_classifier.Utils.load_parameters import *
from intent_classifier.Train.train_model import *

params, model_config = load_parameters()

""" MAIN TRAIN """
Train_Model = TrainModel(params, model_config)
Train_Model.train()


# """If you want to run from outside world, use the following snippet."""
# import json
# import pandas as pd
#
# TRAINING_DATA_CSV = pd.read_csv('/home/juan/CIRA_ML_2/data/ISA_CM_copy1554362050304_TRAIN_DATA.csv', encoding='utf-8')
# TOPICS_DATA_CSV = pd.read_csv('/home/juan/CIRA_ML_2/data/ISA_CM_copy1554362050304_TOPICS.csv', encoding='utf-8')
# SYNONYMS_JSON = '/home/juan/CIRA_ML_2/data/ISA_CM_copy1554362050304_synonyms.json'
# with open(SYNONYMS_JSON) as f:
#     SYNONYMS_JSON = json.load(f)
#
# MAPPED_TOPICS = pd.read_csv('/home/juan/CIRA_ML_2/data/ISA_CM_copy1554362050304_Topic_Mapping_Edited.csv')
#
# MODEL_DIR='/home/juan/CIRA_ML_2/models/'
# LANG='en'
# QUERY_EXPANSION=True
#
# params, model_config = load_parameters(training_csv=TRAINING_DATA_CSV, topics_csv=TOPICS_DATA_CSV,
#                                        synonyms_json=SYNONYMS_JSON, model_dir=MODEL_DIR, lang=LANG,
#                                        mapped_topics=MAPPED_TOPICS, query_expansion=QUERY_EXPANSION)
# Train_Model = TrainModel(params, model_config)
# Train_Model.train()
