from ..Utils.data_handler import *
from ..Utils.synonyms import *
from nltk.util import ngrams
from nltk.tokenize.moses import MosesDetokenizer
from Levenshtein._levenshtein import distance
from collections import Counter
from math import sqrt

import hunspell
import sys
import string
import os
import pickle


class PredictSpellChecker:
    def __init__(self, logger, lang, model_csv_files, nlp=None):

        self.logger = logger
        self.lang = lang
        self.model_csv_files = model_csv_files
        self.nlp = nlp

        self.stop_punctuations = "[" + string.punctuation + "¿`¨´¡" + "]"
        self.stop_punctuations = self.stop_punctuations.replace("$", "")

        self.detokenizer = MosesDetokenizer()

        self.logger.info('\t---> Searching Hunspell Dictionary')

        """ Search dictionary for lang """
        try:
            for dirpath, dirnames, files in os.walk(os.path.abspath('../')):
                if 'hunspell_dictionary' in dirpath:
                    hunsp_dir = dirpath

            dict_file_aff = []
            dict_file_dic = []
            for f_name in os.listdir(hunsp_dir + '/'):
                # Check name of hunspell files.
                if f_name.startswith(self.lang) and f_name.endswith('.aff'):
                    dict_file_aff.append(hunsp_dir + '/' + f_name)
                if f_name.startswith(self.lang) and f_name.endswith('.dic'):
                    dict_file_dic.append(hunsp_dir + '/' + f_name)

            self.logger.info('\t---> Hunspell Dictionary Found')
            self.logger.info('\t---> dict_file_aff = ' + dict_file_aff[0])
            self.logger.info('\t---> dict_file_dic = ' + dict_file_dic[0])

            self.h = hunspell.HunSpell(dict_file_dic[0], dict_file_aff[0])
        except Exception as e:
            self.logger.info(e)
            self.logger.info('\t---> Please, check if you have the spellchecker files for the required language '
                             + self.lang)
            sys.exit()

        dir_ngrams = os.path.join(self.model_csv_files, 'Spell_ngrams.pkl')
        with open(dir_ngrams, 'rb') as ngrams_files:
            self.ngrams_model = pickle.load(ngrams_files)

        """ Read the csv to update hunspell """
        self.words_to_add_df = read_csv(os.path.join(self.model_csv_files, 'Spell_words_to_add.csv'))
        try:
            self.words_to_add_syn_df = read_csv(os.path.join(self.model_csv_files, 'Spell_syns_matcher.csv'))

            """ Build Matcher """
            # Read dictionary of synonyms
            dir_syns_dictionary = os.path.join(self.model_csv_files, 'Spell_syns_dictionary.pkl')
            with open(dir_syns_dictionary, 'rb') as syns_dictionary:
                self.syns_dict = pickle.load(syns_dictionary)

            self.syns_matcher = SynonymsMatcher(self.logger, self.nlp)
            # words_to_add_syn = self.words_to_add_syn_df["Words_Syns"].str.lower().tolist()
            self.syns_matcher = self.syns_matcher.syn_matcher(self.syns_dict)
            self.logger.info('\t---> Synonyms file found.')
        except Exception as e:
            self.syns_matcher = None
            self.syns_dict = None
            self.logger.info('\t---> Synonyms file not found. Synonyms feature is not available')
            pass

    def update_vocabulary(self):
        """ Update HunSpell vocabulary using csv """

        words_to_add = self.words_to_add_df['Words_Add'].str.lower().tolist()

        for word in words_to_add:
            try:
                self.h.add(word.encode(encoding='UTF-8', errors='ignore'))
            except TypeError:
                self.logger.info('null sentence found !')
                pass
            except AttributeError:
                self.logger.info('null sentence found !')
                pass
        return

    def check_words(self, query):
        """ Check words of the user_query """

        query_original = query
        query = query.replace("n't", 'noot')
        doc = self.nlp(query)
        tokens = []
        for tok in doc:
            tokens.append(tok.text)
        for token in range(len(tokens)):
            if 'noot' in tokens[token]:
                tokens[token] = tokens[token].replace("noot", "n't")

        wrong_words = []
        for word in tokens:
            if word.isalpha():
                try:
                    if not self.h.spell(word):
                        wrong_words.append(word)
                except UnicodeEncodeError:
                    self.logger.info("\t---> Character with ascii or latin-1 encoding, please check the dataset it is "
                                     "mandadory use a dataset with utf-8")
                    pass

        if len(wrong_words) == 0:
            return query_original
        else:
            return wrong_words

    def get_suggestions(self, words):
        """a function to reduce the number of suggestions using unigrams
        keyword parameters:
        words:list
        list of suggested words by hunspell
    retu    rn values:
        relevant_suggestions:list
        list of words present in unigram model
        """
        relevant_suggestions = list()
        relevant_suggestions_append = relevant_suggestions.append
        for each_words in ngrams(words, 1):
            try:
                rel_freq = self.ngrams_model[each_words]
                if rel_freq > 0:
                    for each in each_words:
                        relevant_suggestions_append(each)
            except KeyError:
                pass
        return relevant_suggestions

    @staticmethod
    def get_ngrams(tokens, suggested_words, ngram_type):
        """A method to create ngram for given 'n' and filter the relevant ngrams
        keyword arguments:
        tokenized_sentences:list(list)
        list of tokenized sentences
        suggested_words:list
        list of hunspell suggestions for the wrong words
        ngram_type:int
        value of 'N' to choose the ngram
        return values:
        suggested_gram_dict:dict
        a dictionary with ngrams of each suggested words
        """

        n_grams = []
        for tok in tokens:
            n_grams.append(list(ngrams(tok, ngram_type)))
        grams_ = list()
        grams_append = grams_.append
        for each in n_grams:
            for each_gram in each:
                grams_append(each_gram)

        suggested_gram_dict = dict()
        for h_suggest in suggested_words:
            suggested_gram_dict[h_suggest] = []
            for each_bigram in grams_:
                if h_suggest in each_bigram:
                    suggested_gram_dict[h_suggest].append(each_bigram)

        return suggested_gram_dict

    def get_levenshtein_distance(self, wrong_word):
        with open(os.path.join(self.model_csv_files, "unique_train_words.pk"), "rb") as f:
            unique_words = pickle.load(f, encoding='latin1')

        result = {}
        for item in unique_words:
            result[item] = distance(str(wrong_word), str(item))

        return [min(result, key=result.get)]

    @staticmethod
    def word2vec(word):
        # count the characters in word
        cw = Counter(word)
        # precomputes a set of the different characters
        sw = set(cw)
        # precomputes the "length" of the word vector
        lw = sqrt(sum(c * c for c in cw.values()))
        return cw, sw, lw

    @staticmethod
    def cosdis(v1, v2):
        # which characters are common to the two words?
        common = v1[1].intersection(v2[1])
        # by definition of cosine distance we have
        return sum(v1[0][ch] * v2[0][ch] for ch in common) / v1[2] / v2[2]

    def get_max_value_cosine(self, wrong_word):
        with open(os.path.join(self.model_csv_files, "unique_train_words.pk"), "rb") as f:
            unique_words = pickle.load(f, encoding='latin1')

        result = {}
        for item in range(len(unique_words)):
            result[unique_words[item]] = self.cosdis(v1=self.word2vec(word=wrong_word),
                                                     v2=self.word2vec(word=unique_words[item]))
        return [max(result, key=result.get)]

    def correct_spelling(self, wrong_word, query, value_of_n):
        """this function will find the correct word for the wrong word given and insert it into the query
        parameters
        -------
        incorrect_word: str
        incorrect word in the sentence
        query: str
        the sentence given by the user
        value_of_n: int
        value of n to check,2gram through 5gram(default is 2)
        returns
        -------
        ultimate_result:str
        returns the corrected sentence
        """
        suggested_words = self.h.suggest(wrong_word)
        suggested_words += self.get_levenshtein_distance(wrong_word=wrong_word)
        suggested_words += self.get_max_value_cosine(wrong_word=wrong_word)
        suggested_words = list(set(suggested_words))
        suggested_words = self.get_suggestions(suggested_words)

        if len(suggested_words) == 0:
            return query

        elif len(suggested_words) == 1:
            result = suggested_words[0]
            corrected_result = query.replace(wrong_word, result)
            return corrected_result

        elif len(suggested_words) > 1:
            corrected_result = []
            tokens_corrected_result = []
            for word in suggested_words:
                tokens = []
                sentence_replace = query.replace(wrong_word, word)
                corrected_result.append(sentence_replace)
                doc = self.nlp(sentence_replace)
                for tok in doc:
                    tokens.append(tok.text)
                tokens_corrected_result.append(tokens)
        gram_dict = self.get_ngrams(tokens_corrected_result, suggested_words, value_of_n)
        frequency_list = [0] * 20
        for ind, each_hspell in enumerate(suggested_words, 0):
            for _2gram in gram_dict[each_hspell]:
                try:
                    frequency_list[ind] += self.ngrams_model[_2gram]
                except KeyError:
                    pass

        result_index = frequency_list.index(max(frequency_list))
        result = suggested_words[result_index]
        ultimate_result = query.replace(wrong_word, result)
        return ultimate_result

    def predict_spell_checker(self, query_user, value_of_n=2):

        # query_user = query_user.replace("'", "")
        query_user = query_user.replace("n't", 'noot')
        doc = self.nlp(query_user.lower())
        tokens = []

        for tok in doc:
            if tok.text not in self.stop_punctuations:
                tokens.append(tok.text)
        for token in range(len(tokens)):
            if 'noot' in tokens[token]:
                tokens[token] = tokens[token].replace("noot", "n't")

        query = " ".join(tokens)

        if len(query_user) == 0:
            return query, query

        elif value_of_n < 2 or value_of_n > 5:
            raise ValueError("Allowed value of 'n' are 2,3,4,5")

        """ Make the first correction"""
        wrong_words = self.check_words(query)
        if wrong_words == query:
            query = query_user
        else:
            for wrong_word in wrong_words:
                corrected = self.correct_spelling(wrong_word, query_user, int(value_of_n))
                query = corrected

        """ Make the change of synonyms correction"""
        query_syn = query

        if self.syns_matcher:
            doc = self.nlp(query)
            matches = self.syns_matcher(doc)
            syns_found = []
            for match_id, start, end in matches:
                rule_id = self.nlp.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
                span = doc[start: end]  # get the matched slice of the doc
                syns_found.append([span, rule_id])

            for syn in syns_found:
                span = str(syn[0])
                synonym = str(syn[1])
                query_syn = query_syn.replace(span, synonym)                
        else:
            query_syn = query
        return query, query_syn
