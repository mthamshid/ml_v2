from ..Utils.normalizer import *
from ..Utils.json_builder import *
from ..Utils.data_handler import *

from .predict_cosine import *
from .predict_spell_checker import *

from time import time
from datetime import timedelta

import pickle
import os
import spacy

import warnings
warnings.simplefilter("ignore", UserWarning)


class PredictModel:
    def __init__(self, params, model_config=None):
        """ Get parameters (lang, training_csv, url_spellchecker, model_dir, ...) """
        self.lang = params.get('lang')
        self.spell_service = params.get('spell_service', None)
        self.model_dir = params.get('model_dir', './models/')
        self.logger = params.get('logger', None)
        self.clean_topics = params.get('clean_topics')
        self.mapped_topics = params.get('mapped_topics')

        """ Directories to save csv_files and model_files """
        self.model_csv_files = os.path.join(self.model_dir, 'other_files/')
        self.model_model_files = os.path.join(self.model_dir, 'model_files/')
        self.training_files = os.path.join(self.model_dir, 'training_files')

        """ Get configuration of the model (if cosine, if ann, if spell_checker, if save_files) """
        self.cosine = model_config.get('cosine', True)
        self.spell_checker = model_config.get('spell_checker', True)

        """ Load spacy model """
        self.nlp = spacy.load(self.lang, disable=['parser', 'tagger', 'ner'])

        """ Extract Language Variables from params. """
        features_lang = []
        for items in params:
            if self.lang.upper() in items:
                features_lang.append(items)

        """ Create dictionary with Language Variables """
        conf_lang = {}
        for items in features_lang:
            conf_lang[items] = params.get(items)
        self.conf_lang = conf_lang
        self.language = self.conf_lang.get(features_lang[0])

        """ Initializer Spell_Checker"""
        if self.spell_checker:
            self.logger.info("------------------------------------------------------------------------")
            self.logger.info("Initializing Spell_Checker")
            self.logger.info("------------------------------------------------------------------------")
            self.spell_predict = PredictSpellChecker(self.logger, self.lang, self.model_csv_files, self.nlp)
            # We need to update the vocabulary of the spellchecker with training and topics vocabulary
            self.spell_predict.update_vocabulary()

        """Mapping Topics Data Frame None by default"""
        self.mapped_topics_df = None

        """ Get the Normalized dataset """
        self.normalized_data = pd.read_csv(self.model_csv_files + 'Normalized_dataset.csv')

    def load(self):
        self.logger.info("------------------------------------------------------------------------")
        self.logger.info("Loading files")
        self.logger.info("------------------------------------------------------------------------")

        """ Load topics.csv """
        try:
            topics_data_df = pd.read_csv(self.model_csv_files + 'topics.csv', encoding='utf-8')
            self.logger.info('\t---> "topics.csv" found. File loaded.')
        except FileNotFoundError:
            self.logger.info('\t---> "topics.csv" not found. Please check the dir path or training again.')
            sys.exit()

        """ Load language file """
        with open(os.path.join(self.model_model_files, "lang.txt"), "r") as f:
            lang = f.readlines()
            self.lang = lang[0]

        try:
            """ Load synonym file """
            with open(os.path.join(self.training_files, "synonyms_json_training.json"), "r") as f:
                synon_json = json.load(f)
        except FileNotFoundError:
            synon_json = None
            pass

        """Load Topic Mappings"""
        if self.mapped_topics is not None:
            if isinstance(self.mapped_topics, pd.DataFrame):
                self.logger.info("\t---> Reading mapped_topics: (DataFrame)")
                self.mapped_topics_df = self.mapped_topics
            else:
                self.logger.info("\t---> Reading mapped_topics: (CSV) %s" % str(self.mapped_topics))
                self.mapped_topics_df = pd.read_csv(self.mapped_topics)
        else:
            self.mapped_topics_df = None

        """ Initializer classes """
        self.normalizer = Normalizer(topics_data_df, self.conf_lang, synon_json, self.logger, self.lang, \
                                     self.model_csv_files)

        """ Load Cosine files """
        if self.cosine:
            self.logger.info("\t---> Loading Cosine models from directory")
            try:
                with open(os.path.join(self.model_model_files, 'Cosine_tfidf_matrix.mtx'), 'rb') as infile:
                    self.utterance_vector = pickle.load(infile, encoding='latin1')

                with open(os.path.join(self.model_model_files, 'Cosine_vectorizer.pk'), 'rb') as f:
                    self.vectorizer = pickle.load(f, encoding='latin1')

                self.intentions = pd.read_csv(os.path.join(self.model_model_files, 'Cosine_Intentions.csv'))
                self.intentions = self.intentions['Property']

                self.utterances = pd.read_csv(os.path.join(self.model_model_files, 'Cosine_Utterances.csv'))
                self.utterances = self.utterances['Query']

                self.predict_cosine = PredictCosine(self.utterance_vector, self.vectorizer, self.intentions,
                                                    self.utterances)

            except FileNotFoundError:
                self.logger.info("\t---> Cosine model files not found. Please check the dir path or run the training.")
                sys.exit()
            self.logger.info("\t---> Cosine Vectors loaded successfully from " + self.model_model_files)

    def predict(self, query, debug):
        self.logger.info("------------------------------------------------------------------------")
        self.logger.info("Predict started")
        self.logger.info("------------------------------------------------------------------------")

        self.logger.info("\t---> User query: " + query)

        query_original = query

        if self.spell_checker:
            self.logger.info("\t---> Performing Spell Checker")
            query_spell, expanded_corrected = self.spell_predict.predict_spell_checker(query)

            query = expanded_corrected
            self.logger.info("\t---> Query Spell: " + query_spell)
            self.logger.info("\t---> Query Spell Expanded: " + expanded_corrected)

        else:
            query_spell = query
            expanded_corrected = query_spell

            query = query_original

        topics_user = []
        words_topics_user = []

        # Initialize empty dict to display utterances
        dict_output = []

        cleaner_keepstop_lemmatize, sentence_topics, sentence_words_topics = self.normalizer.normalize(query,
                                                                                cleanStopPhrases=False,
                                                                                cleanPoints=True,
                                                                                cleanStopWords=False,
                                                                                lemmatize=True,
                                                                                cleanTopics=False)

        cleaner_removestop_lemmatize, sentence_topics, sentence_words_topics = self.normalizer.normalize(query,
                                                                                                         cleanStopPhrases=True,
                                                                                                         cleanPoints=True,
                                                                                                         cleanStopWords=True,
                                                                                                         lemmatize=True,
                                                                                                         cleanTopics=False)

        remove_all = None
        result_cosine_original, _ = self.predict_cosine.get_result_cosine(cleaner_keepstop_lemmatize[0])

        if float(result_cosine_original[0][1]) >= 0.65 and 'Rivescript_'.lower() in \
                str(result_cosine_original[0][0]).lower() and not cleaner_removestop_lemmatize[0]:

            # KeepStop_Lemmatize
            dict_output.append({
                'Query': cleaner_keepstop_lemmatize[0],
                'Type': 'Keepstop_Lemmatize',
                'IntentionPredicted': None,
                'Prob': None
            })

            # RemoveStop_Lemmatize
            dict_output.append({
                'Query': None,
                'Type': 'Removestop_Lemmatize',
                'IntentionPredicted': None,
                'Prob': None
            })

            # Remove_all
            dict_output.append({
                'Query': remove_all,
                'Type': 'Remove_All',
                'IntentionPredicted': None,
                'Prob': None
            })

            topics_user.append(sentence_topics)
            words_topics_user.append(sentence_words_topics)

        else:
            remove_all, sentence_topics, sentence_words_topics = self.normalizer.normalize(cleaner_removestop_lemmatize[0],
                                                                                            cleanStopPhrases=False,
                                                                                            cleanPoints=False,
                                                                                            cleanStopWords=False,
                                                                                            lemmatize=False,
                                                                                            cleanTopics=True)
            topics_user.append(sentence_topics)
            words_topics_user.append(sentence_words_topics)
            if remove_all[0] == '' and float(result_cosine_original[0][1]) < 0.90:

                remove_all = remove_all[0]

                # KeepStop_Lemmatize
                dict_output.append({
                    'Query': cleaner_keepstop_lemmatize[0],
                    'Type': 'Keepstop_Lemmatize',
                    'IntentionPredicted': None,
                    'Prob': None
                })

                # RemoveStop_Lemmatize
                dict_output.append({
                    'Query': cleaner_removestop_lemmatize[0],
                    'Type': 'Removestop_Lemmatize',
                    'IntentionPredicted': None,
                    'Prob': None
                })

                # Remove_all
                dict_output.append({
                    'Query': remove_all,
                    'Type': 'Remove_All',
                    'IntentionPredicted': None,
                    'Prob': None
                })

            else:
                remove_all = None

                # KeepStop_Lemmatize
                dict_output.append({
                    'Query': cleaner_keepstop_lemmatize[0],
                    'Type': 'Keepstop_Lemmatize',
                    'IntentionPredicted': None,
                    'Prob': None
                })

                # RemoveStop_Lemmatize
                dict_output.append({
                    'Query': cleaner_removestop_lemmatize[0],
                    'Type': 'Removestop_Lemmatize',
                    'IntentionPredicted': None,
                    'Prob': None
                })

                # Remove_all
                dict_output.append({
                    'Query': remove_all,
                    'Type': 'Remove_All',
                    'IntentionPredicted': None,
                    'Prob': None
                })

        queries_user_final = set()
        final_dict_output = []
        for user_query in range(len(dict_output)):
            if dict_output[user_query]['Query'] not in queries_user_final and dict_output[user_query][
                'Query'] is not None:
                queries_user_final.add(dict_output[user_query]['Query'])
                final_dict_output.append(dict_output[user_query])
            else:
                pass

        topics_user_final = set([item for sublist in topics_user for item in sublist])
        words_topics_user = set([item for sublist in words_topics_user for item in sublist])

        topics_user_final = list(topics_user_final)
        words_topics_user = list(words_topics_user)

        for item in final_dict_output:
            self.logger.info("\t---> Normalized querys " + str(item['Query']))

        json = []
        if len(final_dict_output) == 0:
            queries_user_final = ['']

        # Items to display the dict with the modified querys in web service
        types_ = []
        querys_ = []
        for item in final_dict_output:
            querys_.append(item['Query'])
            types_.append(item['Type'])

        modified_querys = dict(zip(types_, querys_))

        # Get the json response for every query
        for x in range(len(final_dict_output)):
            self.logger.info("-----------------------------------------")

            query = final_dict_output[x]['Query']

            if self.cosine:
                cosine_start_time = time()
                result_cosine, result_utterances = self.predict_cosine.get_result_cosine(query)
                cosine_end_time = time()
                # if max_value == '':
                #     pass
                # else:
                #     max_value = self.normalized_data['Query'][max_value]

            self.logger.info("\t---> Cosine result obtained in: %s units." % (
                str(timedelta(seconds=cosine_end_time - cosine_start_time))))

            json_builder = JsonBuilder(self.logger, self.nlp, query_original=query_original, query_modified=query,
                                       query_spell_checker=[query_spell, expanded_corrected], debug_mode=debug,
                                       result_cosine=result_cosine, train_query_output=result_utterances,
                                       model_dir=self.model_dir, list_of_topics=topics_user_final,
                                       list_words_topics=words_topics_user,
                                       queries_to_predict=modified_querys)

            json_obj = json_builder.builder()
            json.append(json_obj)

        self.logger.info("-----------------------------------------")

        prob_int_pred = []
        for items in range(len(json)):
            # Get the intention predicted
            intPred = json[items].get('RelatedIntents')
            # Get the first (highest) result
            intPred_res = intPred[0]
            # Get the literal of the intention predicted
            intPred_class = intPred_res.get('classification')
            # Get the score result of the intention predicted
            intPred_prob = intPred_res.get('prob')
            prob_int_pred.append(intPred_prob)

            final_dict_output[items]['IntentionPredicted'] = intPred_class
            final_dict_output[items]['Prob'] = intPred_prob

        sorted_list_predicts = sorted(final_dict_output, key=lambda i: i['Prob'], reverse=True)

        max_1 = max(prob_int_pred)

        max_item1 = [i for i, x in enumerate(prob_int_pred) if x == max_1]

        json_obj = json[max_item1[0]]

        self.logger.info("\t---> Queries: " + str(queries_user_final))
        for item in sorted_list_predicts:
            self.logger.info("\t---> Final Results: " + str(item['IntentionPredicted']) + " " + str(item['Prob']))

        # UNNBOOSTING
        if len(sorted_list_predicts) > 1 and abs(
                float(sorted_list_predicts[0]['Prob']) - float(sorted_list_predicts[1]['Prob'])) <= 0.02 \
                and str(sorted_list_predicts[0]['IntentionPredicted']).lower() != \
                str(sorted_list_predicts[1]['IntentionPredicted']).lower():

            # If both scores all equals max_item_1 always is json[0], so min score will be json[1]
            if abs(float(sorted_list_predicts[0]['Prob']) - float(sorted_list_predicts[1]['Prob'])) == 0:
                select_json = None
                for item in range(len(json)):
                    if json[item]['ModifiedQuery'] == sorted_list_predicts[1]['Query'] and \
                            float(json[item]['Intent'][0]['prob']) == float(sorted_list_predicts[1]['Prob']):
                        select_json = item
                    else:
                        pass
                conflict_json = json[select_json]
            else:
                if float(sorted_list_predicts[0]['Prob']) > float(sorted_list_predicts[1]['Prob']):
                    for item in range(len(json)):
                        if float(json[item]['Intent'][0]['prob']) == float(sorted_list_predicts[1]['Prob']):
                            select_json = item
                        else:
                            pass
                    conflict_json = json[select_json]
                else:
                    for item in range(len(json)):
                        if float(json[item]['Intent'][0]['prob']) == float(sorted_list_predicts[0]['Prob']):
                            select_json = item
                        else:
                            pass
                    conflict_json = json[select_json]

            # Get the literal of the intention predicted
            conflict_intent_class = conflict_json.get('Intent')[0]['classification']
            conflict_intent_prob = conflict_json.get('Intent')[0]['prob']

            # If the intent to unboost it's in related intents, delete it to avoid duplicates
            row_to_delete = None
            for item in range(len(json_obj['RelatedIntents'])):
                if str(conflict_intent_class) == str(json_obj['RelatedIntents'][item].get('classification')):
                    row_to_delete = json_obj['RelatedIntents'][item]
                else:
                    pass

            if row_to_delete is not None:
                json_obj['RelatedIntents'].remove(row_to_delete)
            else:
                pass

            # Update the json_obj
            # 1. Intent
            json_obj['Intent'].append({
                'classification': conflict_intent_class,
                'prob': conflict_intent_prob
            })
            # 2. ModifiedQuery
            json_obj['ModifiedQuery'] = [json_obj.get('ModifiedQuery'), conflict_json.get('ModifiedQuery')]

            # UnBoosted Json
            json_unboosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            json_obj = json_unboosted.outputs_to_unboosting()

        # BOOSTING
        else:
            # json_dict_mappings = topics_mapping_dict(df_mappings=self.mapped_topics_df)
            # if json_dict_mappings is not None:
            # if str(sorted_list_predicts[0]['Query']) == json_obj.get('ModifiedQuery') and 'Remove_All' \
            #         in str(sorted_list_predicts[0]['Type']):
            #     if 'Unknown_'.lower() in str(json_obj.get('Intent')[0]['classification']).lower():
            #         # Return boosted json
            #         json_boosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            #         json_obj = json_boosted.boost_json()
            #     else:
            #         flag = False
            #         # Stop delete related intents until flag=True, it means that the topic of the predicted intent
            #         # it's associated with a topic mapping
            #         # Save the original json_object if the loop ends without any match in topics
            #         original_json_object = json_obj
            #         while not flag:
            #             any_ = set(json_obj.get('Topics')).intersection(set(\
            #                 json_dict_mappings[json_obj.get('Intent')[0]['classification']]))
            #             if not any_:
            #                 flag = False
            #                 # Take first related intent as new predict intent
            #                 try:
            #                     new_predicted_intent = json_obj.get('RelatedIntents')[0]
            #                 except IndexError:
            #                     flag = True
            #                     json_boosted = JsonBoosted(json_object=original_json_object, debug_mode=debug)
            #                     json_obj = json_boosted.boost_json()
            #
            #                 json_obj['Intent'] = [new_predicted_intent]
            #
            #                 # Delete the first value
            #                 del json_obj.get('RelatedIntents')[0]
            #             else:
            #                 flag = True
            #         # Return cleaned json
            #         json_boosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            #         json_obj = json_boosted.boost_json()
            #
            # else:
            #     # Modified Query it is not Remove_All type
            #     json_boosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            #     json_obj = json_boosted.boost_json()
            # else:
            #     # Mapping topics object is none
            #     json_boosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            #     json_obj = json_boosted.boost_json()
            json_boosted = JsonBoosted(json_object=json_obj, debug_mode=debug)
            json_obj = json_boosted.boost_json()

        self.logger.info("-----------------------------------------")
        self.logger.info("\t---> Web Service Output:")
        self.logger.info("\t--->" + str(json_obj) + "\n")

        return json_obj
