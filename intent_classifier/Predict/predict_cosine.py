from sklearn.metrics.pairwise import cosine_similarity
from collections import OrderedDict
import numpy as np
import pandas as pd


class PredictCosine:
    def __init__(self, utterance_vector, vectorizer, intentions, utterances):
        self.utterance_vector, self.vectorizer, self.intentions, self.utterances = utterance_vector, vectorizer, intentions, utterances

    def get_result_cosine(self, query):
        query_vector = self.vectorizer.transform([query])
        cosine_sim = cosine_similarity(self.utterance_vector,
                                       query_vector)  # Measuring cosine similarity of query with training data.

        # OLD CODE WITH DICTS
        # sim_with_each_intentions = list(zip(self.intentions, cosine_sim))
        # sim_with_each_utterance = list(zip(self.utterances, cosine_sim))
        #
        # sorted_similarity_intentions = sorted(sim_with_each_intentions, key=lambda x: x[1], reverse=True)  # Sorting based
        # sorted_similarity_utterances = sorted(sim_with_each_utterance, key=lambda x: x[1], reverse=True)  # Sorting based

        # Only one max value
        # index_of_maximum = np.where(cosine_sim == cosine_sim.max())
        # if len(index_of_maximum[1]) > 1:
        #     max_value = ''
        # else:
        #     max_value = int(index_of_maximum[0][0])

        cosine_sim = [cosine_sim[i][0] for i in range(len(cosine_sim))]

        temporal_cosine_df = pd.DataFrame({
            'Cosine': cosine_sim,
            'Query': self.utterances,
            'Property': self.intentions
        })
        temporal_cosine_df = temporal_cosine_df.sort_values(by=['Cosine'], ascending=False)

        sim_with_each_intentions = list(zip(temporal_cosine_df['Property'], temporal_cosine_df['Cosine']))
        sim_with_each_utterance = list(zip(temporal_cosine_df['Query'], temporal_cosine_df['Cosine']))

        # with open('sim_with_each_intentions.txt', 'w', encoding='utf-8') as f:
        #     for t in sim_with_each_intentions:
        #         f.write(' '.join(str(s) for s in t) + '\n')
        #
        # with open('sim_with_each_utterance.txt', 'w', encoding='utf-8') as f:
        #     for t in sim_with_each_utterance:
        #         f.write(' '.join(str(s) for s in t) + '\n')

        return sim_with_each_intentions, sim_with_each_utterance
