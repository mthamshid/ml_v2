from ..Utils.data_handler import *
from ..Utils.normalizer import *
from ..Train.train_cosine import *

from sklearn.metrics.pairwise import cosine_similarity
from multiprocessing.dummy import Pool as ThreadPool
import datetime
import time
import itertools
import multiprocessing
import pandas as pd


class ConflictTest:
    def __init__(self, params):
        # Global params
        self.params = params
        # Arguments
        self.lang = params.get('lang')
        self.model_dir = params.get('model_dir')
        self.multithreading = params.get('multithreading')
        self.test_data = params.get('test_data')
        self.clean_topics = params.get("clean_topics")
        self.topics_csv = params.get('topics_csv')
        self.synon_json = params.get("synon_json")
        self.logger = params.get('logger', None)

        """ Extract Language Variables from params. """
        features_lang = []
        for items in params:
            if self.lang.upper() in items:
                features_lang.append(items)

        """ Create dictionary with Language Variables """
        conf_lang = {}
        for items in features_lang:
            conf_lang[items] = params.get(items)
        self.conf_lang = conf_lang
        self.language = self.conf_lang.get(features_lang[0])

        """ Read Topics dataframe """
        if isinstance(self.topics_csv, pd.DataFrame):
            self.topics_data_df = self.topics_csv
            self.logger.info('\t---> First positions of Topics data')
            self.logger.info(('\t--->%s' % self.topics_data_df.head(5)))
        elif self.topics_csv is None:
            self.topics_data_df = None
        else:
            self.topics_data_df = read_csv(self.topics_csv)
            self.logger.info('\t---> First positions of Topics data')
            self.logger.info(('\t--->%s' % self.topics_data_df.head(5)))

        if isinstance(self.synon_json, list):
            pass
        elif self.synon_json:
            with open(self.synon_json) as f:
                self.synon_json = json.load(f)
        else:
            self.synon_json = None

        """ Initializer classes """
        self.normalizer = Normalizer(self.topics_data_df, self.conf_lang, self.synon_json, self.logger, self.lang)
        self.cosine_training = TrainCosine(self.logger)

    def make_tfidf_vectorizer(self, training_df, enabled_multi_threading):
        """ Clean dataframe """
        self.logger.info("\t Normalizer csv")
        clean_df_ = self.normalizer.normalize(training_df, cleanTopics=self.clean_topics, lemmatize=True)

        # clean_df = clean_df.replace(' ', np.NaN)
        clean_df = clean_df_.dropna()

        # Make cosine similiraty, Tfidf and Vectorizer
        utterance_vector, vectorizer, intentions = self.cosine_training.train_cosine(clean_df)

        # Get the index of the each query
        row = range(len(clean_df['Query']))

        if enabled_multi_threading == True:
            # Get a list object with query, vectorizer and row index
            zipped_list = []
            for item in range(len(clean_df['Query'])):
                zipped_list.append([clean_df['Query'][item], vectorizer, utterance_vector, row, row[item]])
            return zipped_list, clean_df
        else:
            return list(clean_df['Query']), vectorizer, utterance_vector, row, clean_df

    def find_conflicts(self, query_, vectorizer_, utterance_vector_, all_rows_, current_row_):
        # Vetorizer input query
        query_vector = vectorizer_.transform([query_])
        # Get cosine similarity between all the utterance and the input query
        cosine_sim = cosine_similarity(utterance_vector_, query_vector)
        # Obtain the sorted similarity
        sim_with_eachIntentions = list(zip(all_rows_, cosine_sim))
        sortedSimilarity = sorted(sim_with_eachIntentions, key=lambda x: x[1],
                                  reverse=True)

        similar_rows_red = []
        similar_rows_orange = []
        similar_rows_yellow = []
        for (intention, sim) in sortedSimilarity:
            if sim[0] >= 0.90:
                similar_rows_red.append(intention + 2)
            elif sim[0] >= 0.75 and sim[0] < 0.90:
                similar_rows_orange.append(intention + 2)
            elif sim[0] >= 0.60 and sim[0] < 0.75:
                similar_rows_yellow.append(intention + 2)

        # Adding row number of the conflict utterance -- RED
        similar_rows_red.append(current_row_ + 2)
        similar_to_rows_unique_red = list(set(similar_rows_red))
        # aux_red.append(sorted(similar_to_rows_unique_red))
        aux_red = sorted(similar_to_rows_unique_red)

        # Adding row number of the conflict utterance -- ORANGE
        similar_rows_orange.append(current_row_ + 2)
        similar_to_rows_unique_orange = list(set(similar_rows_orange))
        # aux_orange.append(sorted(similar_to_rows_unique_orange))
        aux_orange = sorted(similar_to_rows_unique_orange)

        # Adding row number of the conflict utterance -- YELLOW
        similar_rows_yellow.append(current_row_ + 2)
        similar_to_rows_unique_yellow = list(set(similar_rows_yellow))
        # aux_yellow.append(sorted(similar_to_rows_unique_yellow))
        aux_yellow = sorted(similar_to_rows_unique_yellow)

        return aux_red, aux_orange, aux_yellow

    def pack_things_up(self, aux, original_df, cleaned_trainingDf, type):
        aux = [item for item in aux if len(item) > 1]
        aux.sort()
        aux = list(aux for aux, _ in itertools.groupby(aux))
        # Getting a dictionary with bug type and all row numbers in conflict
        dict_aux = dict(enumerate(aux, start=1))
        # Store bug type
        bugType = []
        for i in range(len(dict_aux)):
            bugType.append(type)
        total_conflict_utterances = len(bugType)
        list_of_literals_clean = []
        list_of_intents = []
        list_of_intents_cleaned = []
        for item in aux:
            sub_list = item
            literal_utterances_original = []
            literal_utterances_cleaned = []
            literal_intents_cleaned = []
            for row in sub_list:
                # Get every utterance in conflict
                literal_utterances_cleaned.append(cleaned_trainingDf["Query"][row - 2])
                literal_intents_cleaned.append(cleaned_trainingDf["Property"][row - 2])
                literal_utterances_original.append(original_df["Query"][row - 2])
            # Append every sub list with literal utterances
            list_of_intents.append(literal_utterances_cleaned)
            list_of_intents_cleaned.append(literal_intents_cleaned)
            list_of_literals_clean.append(literal_utterances_original)

        extreme_reds = []
        for list_ in list_of_intents_cleaned:
            set_of_bugs = list(set(list_))
            if len(set_of_bugs) > 1:
                extreme_reds.append(False)
            else:
                extreme_reds.append(True)

        similarQueries = pd.DataFrame(
            {
                'Utterances in Clonficts': list_of_intents,
                'Cleaned - Utterances in Conflits': list_of_literals_clean,
                'Intents in Clonflicts': list_of_intents_cleaned,
                'Similar To Rows': aux,
                'Bug in the same intent': extreme_reds,
                'Bug Type': bugType
            })

        return similarQueries, total_conflict_utterances

    def penalize_conflicts(self, similarQueries, color_type):
        for index, row in similarQueries.iterrows():
            if row['Bug in the same intent'] is False:
                similarQueries.set_value(index, 'Bug Type', color_type)
        return similarQueries

    def show_outputs(self, aux_red, aux_orange, aux_yellow, cleaned_trainingDf, original_df):
        similarQueries_red, total_conflict_utterances_red = self.pack_things_up(aux_red,
                                                                                cleaned_trainingDf,
                                                                                original_df,
                                                                                type="RED")
        # # Convert red to black if the conflicts are on different intents.
        # similarQueries_red = self.penalize_conflicts(similarQueries_red, color_type='RED')


        similarQueries_orange, total_conflict_utterances_orange = self.pack_things_up(aux_orange,
                                                                                      cleaned_trainingDf,
                                                                                      original_df,
                                                                                      type="ORANGE")
        # Convert orange to red if the conflicts are on different intents.
        similarQueries_orange = self.penalize_conflicts(similarQueries_orange, color_type='RED')

        similarQueries_yellow, total_conflict_utterances_yellow = self.pack_things_up(aux_yellow,
                                                                                      cleaned_trainingDf,
                                                                                      original_df,
                                                                                      type="YELLOW")
        # Convert yellow to orange if the conflicts are on different intents.
        similarQueries_yellow = self.penalize_conflicts(similarQueries_yellow, color_type='ORANGE')

        if isinstance(self.test_data, pd.DataFrame):
            filename = '_'
        else:
            filename = get_name_of_file(file_path=self.test_data)

        if total_conflict_utterances_red > 0 or total_conflict_utterances_orange > 0 or total_conflict_utterances_yellow > 0:
            frames = [similarQueries_red, similarQueries_orange, similarQueries_yellow]
            finalDataFrame = pd.concat(frames, ignore_index=True)
            finalDataFrame.to_csv(str(self.model_dir) + 'RESULT_RED_TEST_' + filename + '__' + datetime.datetime.now().strftime(
                '%m%d') + '.csv', sep=",", encoding='utf-8', index=False)
            self.logger.info(str(self.model_dir) + 'RESULT_RED_TEST_%s' % datetime.datetime.now().strftime(
                '%m%d') + '.csv has been created')
            return finalDataFrame
        else:
            self.logger.info("Validation of data set completed. No conflicts found!!!")
            return pd.DataFrame(columns=[])


    def lists_from_results(self, results_objet):
        results_red = []
        results_orange = []
        results_yellow = []

        for file in range(len(results_objet)):
            results_red.append(results_objet[file][0])
            results_orange.append(results_objet[file][1])
            results_yellow.append(results_objet[file][2])

        return results_red, results_orange, results_yellow


    ######### MAIN PART ###########
    def main(self):
        self.logger.info("------------------------------------------------------------------------")
        self.logger.info("-- Conflict TEST --")
        self.logger.info("------------------------------------------------------------------------")

        conflict_test = ConflictTest(self.params)

        # Dataset
        if isinstance(self.test_data, pd.DataFrame):
            df_to_test = self.test_data
        else:
            df_to_test = read_csv(str(self.test_data))

        self.logger.info('Summary of the training data received: ')
        self.logger.info('%s' % df_to_test.describe())

        self.logger.info('Header of test dataframe:')
        self.logger.info('%s' % df_to_test.head(2))


        if self.multithreading is True:
            # Initializing cores and detecting threads.
            cores = multiprocessing.cpu_count()
            pool = ThreadPool(round(cores * 0.70))

            start_time = time.time()

            data, clean_df = conflict_test.make_tfidf_vectorizer(training_df=df_to_test, enabled_multi_threading=True)
            self.logger.info("Performing Multithreading Test")

            # Obtaining all cosine similarities
            results = pool.starmap(conflict_test.find_conflicts, data)

            # Creating a list for every type of result
            sorted_similarity_red, sorted_similarity_orange, sorted_similarity_yellow = conflict_test.lists_from_results(
                results_objet=results)

            # Showing results
            data = conflict_test.show_outputs(aux_red=sorted_similarity_red,
                                       aux_orange=sorted_similarity_orange,
                                       aux_yellow=sorted_similarity_yellow,
                                       cleaned_trainingDf=clean_df,
                                       original_df=df_to_test)

            elapsed_time = time.time() - start_time
            self.logger.info('TIME - MULTITHREADING: ' + str(elapsed_time))
            self.logger.info("Conflicts Detector finished.")
            return data

        else:
            start_time = time.time()

            cleaned_utterances, iterative_vectorizer, iterative_utterance_vector, all_rows, clean_df = conflict_test.make_tfidf_vectorizer(
                training_df=df_to_test,
                enabled_multi_threading=False)

            self.logger.info("Performing normal mode without multithreading Test")

            red_results = []
            orange_results = []
            yellow_results = []

            # Obtaining all cosine similarities
            for item in range(len(cleaned_utterances)):
                sorted_similarity_red, sorted_similarity_orange, sorted_similarity_yellow = conflict_test.find_conflicts(
                    query_=cleaned_utterances[item],
                    vectorizer_=iterative_vectorizer,
                    utterance_vector_=iterative_utterance_vector,
                    all_rows_=all_rows,
                    current_row_=item)
                red_results.append(sorted_similarity_red)
                orange_results.append(sorted_similarity_orange)
                yellow_results.append(sorted_similarity_yellow)

            # Showing results
            data = conflict_test.show_outputs(aux_red=red_results,
                                       aux_orange=orange_results,
                                       aux_yellow=yellow_results,
                                       cleaned_trainingDf=clean_df,
                                       original_df=df_to_test)

            elapsed_time = time.time() - start_time
            self.logger.info('TIME: ' + str(elapsed_time))
            self.logger.info("Conflicts Detector finished.")
            return data
