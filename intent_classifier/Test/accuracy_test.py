import urllib.parse
import multiprocessing
import requests
import datetime
import json as js
import time
from multiprocessing.dummy import Pool as ThreadPool
from ..Utils.data_handler import *
import pandas as pd

class AccuracyTest:
    def __init__(self, params):
        self.url_service = params.get('url_service')
        self.model_dir = params.get('model_dir')
        self.logger = params.get('logger', None)
        self.test_data = params.get('test_data')
        self.multi_thread = params.get('multithreading')
        self.params = params

        self.queryInserted = []
        self.propertyExpected = []
        self.res_wb_json = []
        self.res_wb_cosinePredicted = []
        self.res_wb_neuralPredicted = []
        self.res_wb_intentionPredicted = []
        self.res_wb_spellchecker = []
        self.res_wb_prob = []

        self.CURL = None

    def check_results(self, results, data):

        """CSV COLUMNS"""
        result_final = []
        cosine_result = []
        web_service_result = []
        fails_type = []
        fails_intent_cos1 = []
        output_json = []
        output_spell_corrected = []
        output_query = []
        output_web_service = []
        output_original_cosine = []
        output_expected_intent = []
        # output_topics = []
        output_original_prob = []

        """WEB SERVICE OUTPUTS"""
        res_wb_json = []
        res_intent_classification = []
        res_intent_original_prob = []
        res_intent_prob = []
        res_modified_query = []
        res_spell_corrected = []
        topics_detected = []
        res_wb_cos1 = {}

        """ERROR COUNTERS"""
        error_original_cosine = []
        error_global = []

        for idx in range(len(results)):
            res_query_original = data[idx][0]

            res_property_original = data[idx][1]
            ans_webservice = results[idx]

            # print("DEBUG RESULTS ")
            # print("-------------------------")
            # print("QUERY", res_query_original, "\n")
            # print("PROPERTY", res_property_original, "\n")

            # GENERAL ERROR WS
            try:
                res_wb_json.append(js.dumps(ans_webservice).replace('"Intent":', '\n"Intent":') \
                                   .replace('"Modified Querys":', '\n"Modified Querys":') \
                                   .replace('"ModifiedQuery":', '\n"ModifiedQuery":') \
                                   .replace('"RelatedIntents":', '\n"RelatedIntents":') \
                                   .replace('"SpellCorrect":', '\n"SpellCorrect":') \
                                   .replace('"Topics":', '\n"Topics":'))
            # GENERAL ERROR
            except Exception:
                res_wb_json.append('ERROR')

            # INTENT ERROR
            try:
                res_intent_classification.append(ans_webservice.get('Intent')[0].get('classification'))
                res_intent_original_prob.append(ans_webservice.get('Intent')[0].get('original_prob'))
                res_intent_prob.append(ans_webservice.get('Intent')[0].get('prob'))
            except Exception:
                res_intent_classification.append('ERROR')
                res_intent_original_prob.append('ERROR')
                res_intent_prob.append('ERROR')

            # MODIFIED QUERY
            try:
                res_modified_query.append(ans_webservice.get('ModifiedQuery'))
            except Exception:
                res_modified_query.append('ERROR')

            # RELATED INTENTS
            try:
                res_related_intent = ans_webservice.get('RelatedIntents')
                prob = []
                intent = []
                for items in res_related_intent:
                    prob.append(float(items.get('prob')))
                    intent.append(items.get('classification'))

                if prob.count(1.00) >= 2:
                    for k in range(len(intent)):
                        if prob[k] == 1.00:
                            if idx in res_wb_cos1:
                                res_wb_cos1[idx].append(intent[k])

                            else:
                                res_wb_cos1[idx] = [intent[k]]

            except Exception as e:
                pass

            # SPELL CORRECTED
            try:
                res_spell_corrected.append(ans_webservice.get('SpellCorrect'))
            except Exception:
                res_spell_corrected.append('ERROR')

            # TOPICS DETECTED
            try:
                topics_detected.append(ans_webservice.get('Topics'))
            except Exception:
                topics_detected.append('ERROR')

            try:
                intention_predicted = res_intent_classification[-1]
                prob_pred = res_intent_prob[-1]  # Always 0.90
                prob_cos = res_intent_original_prob[-1]
                spell_pred = res_spell_corrected[-1]
                json = res_wb_json[-1]
            except IndexError:
                pass

            query_original = res_query_original
            expected_intent = res_property_original

            if expected_intent == intention_predicted:
                result_final.append('PASSED')
                # Less than 0.9 - Threshold fail
                if float(prob_cos) < 0.9:
                    # Cosine result without boosting
                    cosine_result.append('FAILED (Threshold)')
                    error_original_cosine.append(1)
                    fails_type.append('Threshold')
                    # Always passed because the result will be boosted
                    web_service_result.append('PASSED')
                # Greather than 0.9 - Passed
                else:
                    cosine_result.append('PASSED')
                    web_service_result.append('PASSED')
                    fails_type.append('-')

                # COSINE INTENTS (More of one 100%)
                if expected_intent in res_wb_cos1:
                    fails_intent_cos1.append(res_wb_cos1.get(expected_intent))
                else:
                    fails_intent_cos1.append('NONE')

            else:
                cosine_result.append('FAILED (Intent)')
                error_original_cosine.append(1)
                web_service_result.append('FAILED (Intent)')
                result_final.append('FAILED')
                error_global.append(1)
                fails_type.append('EXPECTED INTENT')

                # COSINE INTENTS (More of one 100%)
                if expected_intent in res_wb_cos1:
                    fails_intent_cos1.append(res_wb_cos1.get(expected_intent))
                else:
                    fails_intent_cos1.append('NONE')

            output_json.append(json)
            output_spell_corrected.append(spell_pred)
            output_query.append(query_original)
            output_web_service.append(intention_predicted)
            output_original_cosine.append(intention_predicted)
            output_expected_intent.append(expected_intent)
            output_original_prob.append(prob_cos)
            # output_topics.append(topics_detected)

        if isinstance(self.test_data, pd.DataFrame):
            filename = '_'
        else:
            filename = get_name_of_file(file_path=self.test_data)

        if len(error_global) !=0:
            # Accuracy WEB SERVICE
            accuracy_web_service = len(output_query) * [' ']
            accuracy_web_service[0] = round((len(output_query) - len(error_global)) / len(output_query), 3)

            # Accuracy BOOSTED COSINE
            accuracy_boosted_cosine = len(output_query) * [' ']
            accuracy_boosted_cosine[0] = round((len(output_query) - len(error_global)) / len(output_query), 3)

            # Accuracy COSINE
            accuracy_cosine = len(output_query) * [' ']
            accuracy_cosine[0] = round((len(output_query) - len(error_original_cosine)) / len(output_query), 3)

            df = pd.DataFrame({'Query': output_query,
                               'Expected Property': output_expected_intent,
                               'Result Webservice': output_web_service,
                               'Json': output_json,
                               'Result Cosine': output_original_cosine,
                               'Original Prob': output_original_prob,
                               'Cosine Intent with 1': fails_intent_cos1,
                               'Fail cosine': cosine_result,
                               'Fail boosted cosine': web_service_result,
                               'Topics detected': topics_detected,
                               'Spell Checker': output_spell_corrected,
                               'Type Error': fails_type,
                               'RESULT': result_final,
                               'ACCURACY COSINE': accuracy_cosine,
                               'ACCURACY WEBSERVICE': accuracy_web_service,
                               'ACCURACY BOOSTED COSINE': accuracy_boosted_cosine})

            df.to_csv(
                self.model_dir + 'FAILS_test_%s ' % filename + '_' + datetime.datetime.now().strftime('%m%d') + (
                    '.csv'),
                index=False, columns=['Query',
                                      'Expected Property',
                                      'Result Webservice',
                                      'Json',
                                      'Result Cosine',
                                      'Original Prob',
                                      'Cosine Intent with 1',
                                      'Fail cosine',
                                      'Fail boosted cosine',
                                      'Topics detected',
                                      'Spell Checker',
                                      'Type Error',
                                      'RESULT',
                                      'ACCURACY COSINE',
                                      'ACCURACY WEBSERVICE',
                                      'ACCURACY BOOSTED COSINE'])
            self.logger.info('\t' + str(self.model_dir) + 'RESULTS_ACCURACY_TEST_%s' % filename + ('_') + datetime.datetime.now().strftime(
                '%m%d') + '.csv has been created')

        else:
            fails_query = ['NO ERRORS']
            fails_spell = ['NO ERRORS']
            fails_webservice = ['NO ERRORS']
            fails_expected = ['NO ERRORS']
            fails_intent_cos = ['NO ERRORS']
            fails_intent_cos1 = ['NO ERRORS']
            fails_cos = ['NO ERRORS']
            fails_cos_boosted = ['NO ERRORS']
            fails_json_result = ['NO ERRORS']
            fails_type = ['NO ERRORS']
            fails_result = ['NO ERRORS']
            whole_result_cosine = ['NO ERRORS']
            whole_result_cosine[0] = 1
            whole_result_wb = ['NO ERRORS']
            whole_result_wb[0] = 1

            df = pd.DataFrame({'Query': fails_query,
                               'Expected Property': fails_expected,
                               'Result Webservice': fails_webservice,
                               'Json': fails_json_result,
                               'Result Cosine': fails_intent_cos,
                               'Original Prob': output_original_prob,
                               'Cosine Intent with 1': fails_intent_cos1,
                               'Fail cosine': fails_cos,
                               'Fail boosted cosine': fails_cos_boosted,
                               'Topics detected': topics_detected,
                               'Spell Checker': fails_spell,
                               'Type Error': fails_type,
                               'RESULT': fails_result,
                               'ACCURACY COSINE': whole_result_cosine,
                               'ACCURACY WEBSERVICE': whole_result_wb})

            df.to_csv(
                self.model_dir + 'RESULTS_ACCURACY_TEST_%s' % filename + ('_') + datetime.datetime.now().strftime(
                    '%m%d') + ('.csv'),
                index=False, columns=['Query',
                                      'Expected Property',
                                      'Result Webservice',
                                      'Json',
                                      'Result Cosine',
                                      'Original Prob',
                                      'Cosine Intent with 1',
                                      'Fail cosine',
                                      'Fail boosted cosine',
                                      'Topics detected',
                                      'Spell Checker',
                                      'Type Error',
                                      'RESULT',
                                      'ACCURACY COSINE',
                                      'ACCURACY WEBSERVICE',
                                      'ACCURACY BOOSTED COSINE'])
            self.logger.info('\t' + str(self.model_dir) + 'RESULTS_ACCURACY_TEST_%s' % filename +'_' + datetime.datetime.now().strftime(
                '%m%d') + '.csv has been created')

        return

    def url_curl(self, url):
        r = requests.head(url)
        # If URL -> return == False
        if r.status_code == 200:
            return False
        # If CURL -> return == True
        else:
            return True

    def test_webservice(self, query, property_):
        query_original = query
        property_original = property_
        self.logger.info('Procesing sentence %s' %query)

        if self.CURL:
            query = query_original

        else:
            query = urllib.parse.quote(str(query_original).encode('utf-8'))

        # self.logger.info('Procesing sentence HTML FORMAT --> %s' % query)

        ans_webservice = None

        while ans_webservice is None:
            try:
                self.queryInserted = query_original
                self.propertyExpected = property_original
                try:
                    if self.CURL:
                        headers = {
                            'cache-control': 'no-cache',
                            'content-type': 'application/json',
                        }
                        data = '{\n"query":' + '"' + query + '"' + '\n}'
                        response = requests.post(self.url_service, headers=headers, data=data)
                        ans_webservice = response.json()

                    else:
                        ans_webservice = requests.get(self.url_service + '/getIntent?query=' + query + '&debug=True',
                                                      timeout=300).json()

                except Exception as e:
                    self.logger.info("\tFAILED sentence: %s" % query_original)
                    self.logger.info("Web service timed out. Please check if the service is running.")
                    self.logger.info("Retrying the sentence %s" % query_original + "...")
                    time.sleep(2)

            except requests.exceptions.Timeout:
                self.logger.info("\tFAILED sentence: %s" % query_original)
                self.logger.info("Web service timed out. Please check if the service is running.")
                self.logger.info("Retrying the sentence %s" % query_original + "...")
                time.sleep(2)

        return ans_webservice


    def main(self):
        self.logger.info("------------------------------------------------------------------------")
        self.logger.info("-- Accuracy TEST --")
        self.logger.info("------------------------------------------------------------------------")

        test_data_df = read_csv(self.test_data)
        self.logger.info('\t---> First positions of Test Data')
        self.logger.info(('\t--->%s' % test_data_df.head(5)))

        test_data_df["Query"] = test_data_df.Query.str.replace('[^\x00-\x7F]', ' ')
        data_query, data_property = csv_to_list(training_csv=test_data_df)

        data = []
        for items_query, items_property in zip(data_query, data_property):
            data.append([items_query, items_property])

        test_debug = AccuracyTest(self.params)

        CURL = test_debug.url_curl(url=self.url_service)

        if self.multi_thread == True:
            ########## TEST DEBUG MULTITHREADING ###########
            cores = multiprocessing.cpu_count()
            pool = ThreadPool(round(cores * 0.50))

            self.logger.info("\tPerforming Multithreading Test")
            start_time = time.time()

            self.logger.info('\tTotal utterances to process %s' % len(data))

            results = pool.starmap(test_debug.test_webservice, data)
            elapsed_time = time.time() - start_time
            self.logger.info('\tTIME - MULTITHREADING: ' + str(elapsed_time))
            ##################################################
        else:
            ############## TEST DEBUG NORMAL #################
            self.logger.info("\tPerforming Normal Test")
            start_time = time.time()

            results = []
            for item in range(len(data_query)):
                results.append(test_debug.test_webservice(query=data_query[item], property_=data_property[item]))

            elapsed_time = time.time() - start_time
            self.logger.info('\tTIME - NORMAL: ' + str(elapsed_time))
            ##################################################

        # Performing test debug with time out utterances
        start_time = time.time()
        result_test = test_debug.check_results(results, data)
        elapsed_time = time.time() - start_time
        self.logger.info('\tTIME - CHECK RESULTS: ' + str(elapsed_time))
        self.logger.info("\t-- Test done")
