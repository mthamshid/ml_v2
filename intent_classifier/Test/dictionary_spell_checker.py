import requests
import pandas as pd
from multiprocessing.dummy import Pool as ThreadPool
import spacy
import string
import multiprocessing
import time
import sys
import argparse


class SpellcheckerWords:
    def __init__(self, model_dir, training_data):
        self.model_dir = model_dir
        self.training_data = training_data
        self.dataframe = pd.read_csv(self.training_data, encoding='utf-8')
        self.nlp = spacy.load('en', disable=['parser', 'tagger', 'ner'])

        self.url = 'http://18.188.171.107:6002'

    def multi_checker(self, text):
        ans_webservice = requests.get(self.url + '/SpellCorrectionService/SpellCorrect?inputText=' +
                                      text.lower(), timeout=10).json()

        #        ans_webservice_spell = ans_webservice.get('SpellCorrectedText')

        #        return ans_webservice_spell
        return ans_webservice

    def check_words(self, printLog=False):
        try:
            self.queries = self.dataframe.Query.tolist()
        except Exception as e:
            print('Please, check if the dataset has the column "Query"')

        punctuations = list(string.punctuation)

        tokens_dataset = []
        for items in self.queries:
            if '-' in items:
                items = items.replace('-', '')

            doc = self.nlp(items)
            for tok in doc:
                if tok.text not in punctuations:
                    tokens_dataset.append(tok.text.lower())

        set_tokens = set(tokens_dataset)
        print('Number of unique tokens: ', len(set_tokens))

        cores = multiprocessing.cpu_count()
        pool = ThreadPool(round(cores * 0.75))
        results = pool.map(self.multi_checker, set_tokens)

        #        print('RESULTS', results)
        print('Number of corrected unique tokens: ', len(results))

        corrected_words = []
        list_set_tokens = list(set_tokens)
        for idx in range(len(results)):
            result_idx = results[idx]
            word_asked = list_set_tokens[idx]
            expanded = result_idx.get('ExpandedQuery')
            spell_correction = result_idx.get('SpellCorrectedText')

            if expanded == word_asked:
                if word_asked != spell_correction:
                    corrected_words.append(word_asked)
            else:
                pass

        print('Number of words not in dictionary: ', len(corrected_words))
        df = pd.DataFrame({'NO ENGLISH WORDS': corrected_words})

        df.to_csv(self.model_dir + 'SPELL_DICTIONARY_ ' + self.training_data + '.csv', index=False)

        return


if __name__ == "__main__":

    #    model_dir = "D:/Cognicor/Code/PRUEBAS/"
    #    dataset = 'wsp_sept_100918_OLD.csv'
    #    project = 'WSP'
    parser = argparse.ArgumentParser(description='Test the webservice with a new dataset')


    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')


    defaul_debug = True
    defaul_CURL = False
    parser.add_argument('--model_dir', metavar='model_dir', help='Directory')
    parser.add_argument('--dataset', metavar='dataset', help='Dataset')

    args = parser.parse_args()

    if not args.model_dir:
        print()
        print('Please, insert the directory')
        print()
        print(parser.print_help())
        sys.exit()

    if not args.dataset:
        print()
        print('Please, insert the dataset')
        print()
        print(parser.print_help())
        sys.exit()

    params = {
        'model_dir': args.model_dir,
        'dataset': args.dataset,
    }
    #
    #    params = {
    #        'model_dir': model_dir,
    #        'dataset': dataset,
    #    }
    #
    model_dir = params.get('model_dir')
    dataset = params.get('dataset')

    start_time = time.time()

    print('Searching NO english words...')
    Spellchecker = SpellcheckerWords(model_dir, dataset)
    training_data_df = Spellchecker.check_words()
    print('Search done')
    elapsed_time = time.time() - start_time
    print('TIME', elapsed_time)
