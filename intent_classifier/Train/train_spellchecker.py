﻿import os
import string
import pickle
import collections
from nltk.util import ngrams

from ..Utils.data_handler import save_csv, csv_to_pandas


class TrainSpellChecker:
    def __init__(self, logger, lang, model_csv_files=None, nlp=None):
        self.logger = logger
        self.lang = lang
        self.model_csv_files = model_csv_files

        self.stop_punctuations = "[" + string.punctuation + "¿`¨´¡" + "]"
        self.stop_punctuations = self.stop_punctuations.replace("$", "")

        self.nlp = nlp

    def build_words_to_add(self, words_training, topics_df, words_synon=None):
        """ Generate a csv with words to add """

        # # Take every word in json
        if words_synon is not None:
            synonymous = []
            for key, value in words_synon.items():
                synonymous.append(value)

            synonymous = [item for sublist in synonymous for item in sublist]

        words_training = list(set(words_training))

        # Take Topic and Variation columns to add them
        topics = topics_df.filter(["TOPIC"], axis=1)
        topics = topics.dropna()
        topics = topics["TOPIC"].str.lower().tolist()

        variation = topics_df.filter(["VARIATION"], axis=1)
        variation = variation.dropna()
        variation = variation["VARIATION"].str.lower().tolist()

        if words_synon is not None:
            topics_list = list(set(topics + variation + synonymous))
        else:
            topics_list = list(set(topics + variation))

        tokens_topics = []
        for topic in topics_list:
            doc = self.nlp(topic)
            toks = []
            for tok in doc:
                if tok.text not in self.stop_punctuations:
                    toks.append(tok.text)
            tokens_topics = tokens_topics + toks

        tokens_topics = list(set(tokens_topics))

        # Take a set of words
        words_to_add = list(set(words_training + tokens_topics))

        if self.lang == "en":
            special_words = [
                "'t've",
                "'d've",
                "'ve",
                "'cause",
                "'t",
                "n't",
                "'er",
                "'s",
                "'d",
                "'ll",
                "'ve",
                "'re",
                "'m",
                "'m'a",
                "I'm'o",
                "'clock",
                "'tis",
                "'twas",
                "'all",
                "$",
                "€"
            ]

            words_to_add = list(set(words_to_add + special_words))

        # Save words to add
        args = (["Words_Add"], [words_to_add])
        words_to_add_tok_df = csv_to_pandas(*args)
        save_csv(words_to_add_tok_df, self.model_csv_files, name_file="Spell_words_to_add")

        if words_synon is not None:
            args = (["Words_Syns"], [synonymous])
            words_to_add_syn_df = csv_to_pandas(*args)
            save_csv(words_to_add_syn_df, self.model_csv_files, name_file="Spell_syns_matcher")
        return

    def ngram_syns(self, ngrams, synonymous):
        ngram_variation = {}

        for key, value in ngrams.items():
            for k in key:
                if k in synonymous:
                    syns = synonymous.get(k)
                    for syn in syns:
                        ngram_list = list(key)
                        indexs = [i for i, x in enumerate(ngram_list) if x == k]
                        for idx in indexs:
                            ngram_replace = ngram_list
                            ngram_replace[idx] = syn
                            ngram_replace_tuple = tuple(ngram_replace)
                            ngram_variation[ngram_replace_tuple] = value
        return ngram_variation

    def build_ngrams(self, training_df, synon_json=None,reuse=False):
        """ Generate a pickle with ngrams """

        query_list = training_df["Query"].str.lower().tolist()

        # Get words_to_syns
        if synon_json is not None:
            synon_to_word = {}
            for syns in synon_json:
                value = syns.get("value")
                syns_word = syns.get("synonyms")
                synon_to_word[value] = syns_word

            with open(
                os.path.join(self.model_csv_files, "Spell_syns_dictionary.pkl"), "wb"
            ) as f:
                pickle.dump(synon_to_word, f, pickle.HIGHEST_PROTOCOL)

        query_tok_ngrams = []
        query_tok_add = []
        for sentence in query_list:
            doc = self.nlp(sentence)
            for token in doc:
                if token.text not in self.stop_punctuations:
                    query_tok_ngrams.append(token.text)
                    if token.text is not token.text.isdigit():
                        query_tok_add.append(token.text)
                else:
                    continue

        Unigrams = ngrams(query_tok_ngrams, 1)
        Bigrams = ngrams(query_tok_ngrams, 2)
        Trigrams = ngrams(query_tok_ngrams, 3)

        ngrams_freq = dict()
        Unigrams_freq = dict(collections.Counter(Unigrams))
        ngrams_freq.update(Unigrams_freq)

        Bigrams_freq = dict(collections.Counter(Bigrams))
        ngrams_freq.update(Bigrams_freq)

        Trigrams_freq = dict(collections.Counter(Trigrams))
        ngrams_freq.update(Trigrams_freq)

        if synon_json is not None:
            ngrams_syns = self.ngram_syns(ngrams_freq, synon_to_word)

            ngrams_freq_copy = ngrams_freq.copy()
            ngrams_freq_copy.update(ngrams_syns)

            ngram_file = open(
                os.path.join(self.model_csv_files, "Spell_ngrams.pkl"), "wb"
            )
            pickle.dump(ngrams_freq_copy, ngram_file)

            return query_tok_add, synon_to_word

        else:
            ngram_file = open(
                os.path.join(self.model_csv_files, "Spell_ngrams.pkl"), "wb"
            )
            pickle.dump(ngrams_freq, ngram_file)

            return query_tok_add, None
            if reuse: return ngrams_freq


    def train_spell_checker(self, training_df, topics_df, synon_json=None):
        # Build ngrams
        words_training, words_synonyms = self.build_ngrams(
            training_df, synon_json=synon_json
        )

        # Build words to add to hunspell
        self.build_words_to_add(words_training, topics_df, words_synon=words_synonyms)
    """
    def update_ngram(self,query_log_df):

        ngram_file = open(os.path.join(self.model_csv_files, "Spell_ngrams.pkl"), "wb")
        new_frequencies=self.build_ngrams(training_df=query_log_df, reuse=True)
        old_frequencies=dict(ngram_file)
        old_frequencies.update(new_frequencies)
        pickle.dump(old_frequencies, ngram_file)
        self.logger.info("n-gram frequencies are updated")
    """