from sklearn.feature_extraction.text import TfidfVectorizer
import sys


class TrainCosine:
    def __init__(self, logger):
        self.logger = logger

    def train_cosine(self, training_df):
        intentions = training_df.loc[:, "Property"].values.astype("U")
        utterances = training_df.loc[:, "Query"].values.astype("U")

        try:
            vectorizer = TfidfVectorizer(stop_words=None)
            utterance_vector = vectorizer.fit_transform(utterances)
        except ValueError:
            self.logger.info("Training Data contains empty rows. Please clean it.")
            sys.exit()

        return utterance_vector, vectorizer, intentions, utterances
