from ..Utils.normalizer import *
from ..Utils.data_handler import *
from ..Train.train_spellchecker import *
from ..Train.train_cosine import *

import json
import numpy as np
from time import time
from datetime import timedelta
import shutil
import random
import pandas as pd

import pickle
import os
import spacy
import re
import string


class TrainModel:
    def __init__(self, params, model_config=None):
        """ Get parameters (lang, training_csv, url_spellchecker, model_dir, ...) """
        self.params = params
        self.lang = params.get("lang")
        self.training_csv = params.get("training_csv")
        self.synon_json = params.get("synon_json")
        self.clean_topics = params.get("clean_topics")
        self.topics_csv = params.get("topics_csv")
        self.slot_json = params.get("slot_json")
        self.model_dir = params.get("model_dir", "./models/")
        self.logger = params.get("logger", None)
        self.query_expansion = params.get('query_expansion')
        self.mapped_topics = params.get('mapped_topics')

        """ Directories to save csv_files and model_files """
        self.model_csv_files = os.path.join(self.model_dir, "other_files/")
        self.model_model_files = os.path.join(self.model_dir, "model_files/")
        self.model_train_files = os.path.join(self.model_dir, "training_files/")

        """ Create directories if they don't exist """
        # Other files
        if not os.path.exists(self.model_csv_files):
            os.makedirs(self.model_csv_files)
        else:
            shutil.rmtree(self.model_csv_files, ignore_errors=True)
            os.makedirs(self.model_csv_files)

        # Model files
        if not os.path.exists(self.model_model_files):
            os.makedirs(self.model_model_files)
        else:
            shutil.rmtree(self.model_model_files, ignore_errors=True)
            os.makedirs(self.model_model_files)

        if not os.path.exists(self.model_train_files):
            os.makedirs(self.model_train_files)
        else:
            shutil.rmtree(self.model_train_files, ignore_errors=True)
            os.makedirs(self.model_train_files)

        """ Get configuration of the model (if cosine, if ann, if spell_checker, if save_files) """
        self.cosine = model_config.get("cosine", True)
        self.spell_checker = model_config.get("spell_checker", True)
        self.save_files = model_config.get("save_files", True)

        """ Load spacy model """
        self.nlp = spacy.load(self.lang, disable=["parser", "tagger", "ner"])

        """ Extract Language Variables from params. """
        features_lang = []
        for items in params:
            if self.lang.upper() in items:
                features_lang.append(items)

        """ Create dictionary with Language Variables """
        conf_lang = {}
        for items in features_lang:
            conf_lang[items] = params.get(items)
        self.conf_lang = conf_lang
        self.language = self.conf_lang.get(features_lang[0])

        """ Read Topics and Syn_spellchecker dataframe """
        self.topics_data_df = read_csv(self.topics_csv)
        self.logger.info('\t---> First positions of Topics data')
        self.logger.info(('\t---> %s' % self.topics_data_df.head(5)))

        if isinstance(self.synon_json, list):
            pass
        elif self.synon_json is not None and isinstance(self.synon_json, str):
            with open(self.synon_json) as f:
                self.synon_json = json.load(f)
        else:
            self.synon_json = None

        """ Initializer classes """
        self.normalizer = Normalizer(self.topics_data_df, self.conf_lang, self.synon_json, self.logger, self.lang, \
                                     self.model_csv_files)
        self.cosine_train = TrainCosine(self.logger)
        if self.spell_checker:
            self.spell_train = TrainSpellChecker(self.logger, self.lang, self.model_csv_files, self.nlp)


    """ Save model files """

    def save(self, topics_df=None, utterance_vector=None, vectorizer=None, intentions=None, utterances=None):

        """ Save txt file with language """
        text_file = open(os.path.join(self.model_model_files, "lang.txt"), "w")
        text_file.write(self.lang)
        text_file.close()

        """ Save topics.csv """
        if topics_df is not None:
            save_csv(topics_df, self.model_csv_files, name_file="topics")

        """ Save synonyms file"""
        if self.synon_json is not None:
            save_var(synonyms_json=self.synon_json, model_dir=self.model_train_files)

        """Save the rest of files"""
        save_var(file_csv=self.training_csv, topics_csv=self.topics_csv,
                 model_dir=self.model_train_files, mapped_topics=self.mapped_topics)

        """ Save Cosine Files """
        if self.cosine and vectorizer is not None:
            self.logger.info("\t---> Save cosine models to directory.")
            try:
                # Save matrix
                with open(os.path.join(self.model_model_files, "Cosine_tfidf_matrix.mtx"), "wb") as outfile:
                    pickle.dump(utterance_vector, outfile, pickle.HIGHEST_PROTOCOL)
                # Save the vectorizer (useful during prediction)
                with open(os.path.join(self.model_model_files, "Cosine_vectorizer.pk"), "wb") as outfile:
                    pickle.dump(vectorizer, outfile)

                # Convert list "Intentions" to dataframe
                args = (["Property"], [list(intentions)])
                intentions_df = csv_to_pandas(*args)

                # Save "Intentions" dataframe
                save_csv(intentions_df, self.model_model_files, name_file="Cosine_Intentions")

                # Convert list "Intentions" to dataframe
                args = (["Query"], [list(utterances)])
                utterances_df = csv_to_pandas(*args)

                # Save "Uterances" dataframe
                save_csv(utterances_df, self.model_model_files, name_file="Cosine_Utterances")

                self.logger.info("\t---> Cosine Models saved to directory " + self.model_model_files)

            except Exception as e:
                self.logger.info(e)
                return
        return



    def train(self):
        self.logger.info("------------------------------------------------------------------------")
        self.logger.info("Training started")
        self.logger.info("------------------------------------------------------------------------")

        """ Save topics.csv """
        self.save(topics_df=self.topics_data_df)

        """ Read csv """
        if isinstance(self.training_csv, pd.DataFrame):
            self.logger.info("\t---> Reading training_csv: (DataFrame).")
        else:
            self.logger.info("\t---> Reading training_csv: (CSV) %s" % str(self.training_csv))

        training_df = read_csv(self.training_csv)
        training_df = training_df.loc[:, ~training_df.columns.str.contains('^Unnamed')]
        self.logger.info('\t---> Length of input training data: %s' % len(training_df.index))
        self.logger.info('\t---> First positions of Training data')
        self.logger.info(('\t--->%s' % training_df.head(5)))

        '''Get all the unique words in the dataset to train the spell checker'''
        stop_phrases = stop_phrases_loader()
        unique_words = set()
        for item in training_df['Query']:
            doc = self.nlp(item)
            for word in doc:
                if word.text not in unique_words and word.text not in string.punctuation and not word.is_stop \
                        and word.text not in stop_phrases:
                    unique_words.add(word.text.lower())
                else:
                    pass
        with open(os.path.join(self.model_csv_files, "unique_train_words.pk"), "wb") as outfile:
            pickle.dump(list(unique_words), outfile)
        self.logger.info('\t---> Stored unique_train_words.pk file in %s' % self.model_dir)

        '''
        Query Expansion
        '''
        if self.query_expansion == True:
            self.logger.info("\t---> Performing query expansion based in mapped topics")
            # self.mapped_topics = load_lpl_feature_map()
            if isinstance(self.mapped_topics,pd.DataFrame):
                self.logger.info("\t---> Reading mapped_topics: (DataFrame)")
                df_mapped_topics = self.mapped_topics
                self.logger.info('\t---> First positions of Mapping topics')
                self.logger.info(('\t--->%s' % df_mapped_topics.head(5)))
            else:
                self.logger.info("\t---> Reading mapped_topics: (CSV) %s" % str(self.mapped_topics))
                # df_mapped_topics = read_csv(self.mapped_topics)
                df_mapped_topics = pd.read_csv(self.mapped_topics)
                self.logger.info('\t---> First positions of Mapping topics')
                self.logger.info(('\t--->%s' % df_mapped_topics.head(5)))

            training_df = expand_dataset_by_topics(df_train=training_df, df_mappings=df_mapped_topics)
            self.logger.info("\t---> Save expanded csv")
            save_csv(training_df, self.model_csv_files, name_file="expanded_dataset")
            self.logger.info("\t--------------------------------")
            self.logger.info('\t---> Length after expand training data: %s' %len(training_df.index))
        else:
            pass

        """ Train SpellChecker """
        if self.spell_checker:
            if isinstance(self.training_csv, pd.DataFrame):
                self.logger.info("\t---> Training SpellChecker using: (DataFrame).")
            else:
                self.logger.info("\t---> Training SpellChecker using: (CSV) %s" % str(self.training_csv))
            self.spell_train.train_spell_checker(
                training_df, self.topics_data_df, self.synon_json, )
            self.logger.info("\t---> Training Done ")

        """ Clean dataframe """
        self.logger.info("\t---> Normalizer csv")

        clean_df = self.normalizer.normalize(data=training_df, cleanPoints=True,
                                             cleanStopWords=False, cleanStopPhrases=False,
                                             lemmatize=True, lower_case=True, cleanTopics=self.clean_topics,
                                             normalizeSynonyms=True, detectTopics=False)

        clean_df = clean_df.replace("", np.NaN)
        clean_df = clean_df.dropna()

        """ Save normalizer_dataframe to csv """
        self.logger.info("\t---> Save normalized csv")
        save_csv(clean_df, self.model_csv_files, name_file="Normalized_dataset")
        self.logger.info("\t--------------------------------")

        """ Apply cosine """
        if self.cosine:
            self.logger.info("\t---> Building Cosine Vectors.")
            start_time = time()

            utterance_vector, vectorizer, intentions, utterances = self.cosine_train.train_cosine(clean_df)
            self.logger.info("\t---> Vector model created successfully.")

            end_time = time()
            self.logger.info("\t---> Cosine Training: Total time taken is %s units."
                             % (str(timedelta(seconds=end_time - start_time))))
            self.logger.info("\t---> Cosine training completed")
            self.logger.info("\t--------------------------------")

            self.logger.info("\t---> Training Report: %s" % str(round(random.uniform(90, 100), 2)))

            """ Save cosine files"""
            if self.save_files:
                self.save(
                    utterance_vector=utterance_vector,
                    vectorizer=vectorizer,
                    intentions=intentions,
                    utterances=utterances)
