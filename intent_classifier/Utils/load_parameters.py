import logging
import os
import argparse
import sys
import pandas as pd
from logging.handlers import TimedRotatingFileHandler
from ..Utils.data_handler import *

global params
global model_config


""" Create directories if they don't exist """
logs_dir = './logs/'

if not os.path.exists(logs_dir):
    os.makedirs(logs_dir)

def load_parameters(training_csv=None, topics_csv=None, synonyms_json=None, model_dir=None, lang=None,
                    clean_topics=None, port=None, test_data=None, url_service=None, multithreading=None,
                    mapped_topics=None, query_expansion=None):

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    # model_dir is common parameter for all the process in EXTERNAL_PARAMETERS configuration
    if model_dir is not None:
        '''EXTERNAL_PARAMETERS'''
        try:
            '''TRAIN'''
            if training_csv is not None and lang is not None and topics_csv is not None:
                if synonyms_json is not None:
                    pass
                else:
                    synonyms_json = None

                if clean_topics is not None:
                    pass
                else:
                    # Dirty mode by default
                    clean_topics = False

                if mapped_topics is not None:
                    pass
                else:
                    mapped_topics = None

                if query_expansion is not None:
                    pass
                else:
                    query_expansion = False

                mode = ['EXTERNAL_VARIABLES', 'TRAIN', training_csv, lang, model_dir, topics_csv, synonyms_json, clean_topics,
                        mapped_topics, query_expansion]
            else:
                # Error in mandatory arguments
                pass
        except Exception as e:
            pass

        try:
            '''PREDICT'''
            if port is not None:
                # Languaje
                if lang is not None:
                    pass
                else:
                    sys.exit()

                # clean_topics
                if clean_topics is not None:
                    pass
                else:
                    # Dirty mode by default
                    clean_topics = False

                # mapped_topics
                if mapped_topics is not None:
                    pass
                else:
                    mapped_topics = None

                mode = ['EXTERNAL_VARIABLES', 'PREDICT', lang, port, clean_topics, model_dir, mapped_topics]
            else:
                # Error in mandatory arguments
                pass
        except Exception as e:
            pass

        try:
            '''TEST'''
            # Common argumments
            if test_data is not None and model_dir is not None:
                # Discriminant arguments
                if url_service is not None and topics_csv is not None:
                    print('url_service should be None or topics_csv or lang')
                    sys.exit()
                else:
                    pass

                # Optional argumetn Multithreading
                if multithreading is None:
                    multithreading = False
                else:
                    pass

                if synonyms_json is not None:
                    pass
                else:
                    synonyms_json = None

                mode = ['EXTERNAL_VARIABLES', 'TEST', test_data, model_dir, lang, topics_csv,
                                                      multithreading, url_service, synonyms_json]
            else:
                # Error in common mandatory arguments
                pass
        except Exception as e:
            pass

    else:
        """ENVIRONMENT VARIABLES"""
        mode = ['ENV']

        """ Train """
        # We need the next arguments: dataset, lang, model_dir, topics, synonyms
        # Get training data
        try:
            if 'TRAINING_DATA_CSV' in os.environ:
                training_csv = os.environ['TRAINING_DATA_CSV']
                # Get lang
                if os.environ['LANG']:
                    lang = os.environ['LANG']
                # Get model_dir
                if os.environ['MODEL_DIR']:
                    model_dir = os.environ['MODEL_DIR']
                # Get topics_data_csv
                if 'TOPICS_DATA_CSV' in os.environ:
                    topics_csv = os.environ['TOPICS_DATA_CSV']
                # Get synonyms_json. If not, synonyms_json = None
                try:
                    if os.environ['SYNONYMS_JSON'] == 'None':
                        synonyms_json = None
                    else:
                        synonyms_json = os.environ['SYNONYMS_JSON']
                except Exception as e:
                    synonyms_json = None
                # Get clean_topics. If not, clean_topics = True
                try:
                    clean_topics = str2bool(os.environ['CLEAN_TOPICS'])
                    # Dirty mode by default
                    clean_topics = False
                except Exception as e:
                    clean_topics = False

                # Get mapped_topics. If not, mapped_topics = None
                try:
                    if os.environ['MAPPED_TOPICS'] == 'None':
                        mapped_topics = None
                    else:
                        mapped_topics = os.environ['MAPPED_TOPICS']
                except Exception as e:
                    mapped_topics = None

                # Get query_expansion. If not, query_expansion = False
                try:
                    if mapped_topics is None:
                        query_expansion = False
                    else:
                        query_expansion = str2bool(os.environ['QUERY_EXPANSION'])
                except Exception as e:
                    query_expansion = False

                mode = ['ENV', 'TRAIN', training_csv, lang, model_dir, topics_csv, synonyms_json, clean_topics,
                        mapped_topics, query_expansion]
            else:
                pass
        except Exception as e:
            pass

        """ Predict """
        # We need the next arguments: model_dir, lang, port
        # Get model_dir
        try:
            if os.environ['PORT']:
                port = os.environ['PORT']
                if os.environ['MODEL_DIR']:
                    model_dir = os.environ['MODEL_DIR']
                # Get  lang
                if os.environ['LANG']:
                    lang = os.environ['LANG']
                try:
                    clean_topics = str2bool(os.environ['CLEAN_TOPICS'])
                    # Dirty mode by default
                    clean_topics = False
                except Exception as e:
                    clean_topics = False

                # Get mapped_topics. If not, mapped_topics = None
                try:
                    if os.environ['MAPPED_TOPICS'] == 'None':
                        mapped_topics = None
                    else:
                        mapped_topics = os.environ['MAPPED_TOPICS']
                except Exception as e:
                    mapped_topics = None

                mode = ['ENV', 'PREDICT', lang, port, clean_topics, model_dir, mapped_topics]
            else:
                pass
        except Exception as e:
            pass

        """ Test """
        # We need the next arguments: model_dir, lang, port, topics_csv, test_data, url_service, multithreading
        try:
            # Get test_data
            if os.environ['TEST_DATA']:
                test_data = os.environ['TEST_DATA']
                # Get lang
                if os.environ['LANG'] == 'None':
                    lang = None
                else:
                    lang = os.environ['LANG']
                # Get model_dir
                if os.environ['MODEL_DIR']:
                    model_dir = os.environ['MODEL_DIR']
                # Get topics_data_csv
                try:
                    if os.environ['TOPICS_DATA_CSV'] == 'None':
                        topics_csv = None
                    else:
                        topics_csv = os.environ['TOPICS_DATA_CSV']
                except Exception as e:
                    topics_csv = None
                # Multithreading mode option
                if os.environ['ENABLE_MULTITHREADING']:
                    multithreading = str2bool(os.environ['ENABLE_MULTITHREADING'])
                else:
                    multithreading = False
                # Get url_service
                try:
                    if os.environ['URL_SERVICE'] == 'None':
                        url_service = None
                    else:
                        url_service = os.environ['URL_SERVICE']
                except Exception as e:
                    url_service = None
                # Get synonyms_json
                try:
                    if os.environ['SYNONYMS_JSON'] == 'None':
                        synonyms_json = None
                    else:
                        synonyms_json = os.environ['SYNONYMS_JSON']
                except Exception as e:
                    synonyms_json = None

                mode = ['ENV', 'TEST', test_data, lang, model_dir, topics_csv, multithreading, url_service, synonyms_json]

            else:
                pass
        except Exception as e:
            pass

    """ Load Logger """
    try:
        if mode[1] == 'TRAIN':
            logHandler = TimedRotatingFileHandler(os.path.join(logs_dir, "training_phase"), when="d", interval=1, encoding='UTF-8')
            logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logHandler.setFormatter(logFormatter)
            logger = logging.getLogger('Main_Train')

        elif mode[1] == 'PREDICT':
            logHandler = TimedRotatingFileHandler(os.path.join(logs_dir, "webservice_phase"), when="d", interval=1, encoding='UTF-8')
            logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logHandler.setFormatter(logFormatter)
            logger = logging.getLogger('Main_Webservice')

        elif mode[1] == 'TEST':
            logHandler = TimedRotatingFileHandler(os.path.join(logs_dir, "testing_phase"), when="d", interval=1, encoding='UTF-8')
            logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logHandler.setFormatter(logFormatter)
            logger = logging.getLogger('Main_Test')
        else:
            pass

        logger.addHandler(logHandler)
        logger.setLevel(logging.INFO)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        logger.addHandler(consoleHandler)

        if mode[0] == 'ENV':
            logger.info("Environment variables detected.")
        elif mode[0] == 'EXTERNAL_VARIABLES':
            logger.info('External parameters detected.')
        else:
            pass


    except Exception as e:
        if mode[0] == 'ENV':
            sys.stderr.write('ERROR\n')
            sys.stderr.write("There are not enough arguments in the environment file. Please, check it. You need the next"
                             " parameters: \n")
            sys.stderr.write("\t---> TRAIN:  TRAINING_DATA_CSV, MODEL_DIR, LANG, TOPICS_DATA_CSV, SYNONYMS_JSON"
                             " (Optional), CLEAN_TOPICS (Optional), MAPPED_TOPICS (Optional), QUERY_EXPANSION (Optional)\n")
            sys.stderr.write(
                "\t---> PREDICT:  MODEL_DIR, LANG, PORT, CLEAN_TOPICS (Optional), MAPPING_TOPICS (Optional)\n")
            sys.stderr.write("\t---> TEST:  \n"
                             "\t\t\tMODE 1 - CONFLICT_TEST: TEST_DATA, MODEL_DIR, LANG, TOPICS_DATA_CSV, CLEAN_TOPICS, "
                             "ENABLE_MULTITHREADING "
                             " (Optional)\n"
                             "\t\t\tMODE 2 - ACCURACY_TEST: TEST_DATA, MODEL_DIR, URL_SERVICE, ENABLE_MULTITHREADING "
                             "(Optional)\n")

        if mode[0] == 'EXTERNAL_VARIABLES':
            sys.stderr.write('ERROR\n')
            sys.stderr.write(
                "There are not variables. Please, check it. You need the next parameters: \n")
            sys.stderr.write("\t---> TRAIN:  TRAINING_DATA_CSV, MODEL_DIR, LANG, TOPICS_DATA_CSV, SYNONYMS_JSON"
                             " (Optional), CLEAN_TOPICS (Optional), MAPPED_TOPICS (Optional), QUERY_EXPANSION (Optional)\n")
            sys.stderr.write(
                "\t---> PREDICT:  MODEL_DIR, LANG, PORT, CLEAN_TOPICS (Optional), MAPPING_TOPICS (Optional)\n")
            sys.stderr.write("\t---> TEST:  \n"
                             "\t\t\tCONFLICT_TEST: TEST_DATA, MODEL_DIR, LANG, TOPICS_DATA_CSV, "
                             "ENABLE_MULTITHREADING "
                             " (Optional)\n")

        sys.exit()

    """ Detect mode: Train / Predict/ Test """
    if mode[1] == 'TRAIN':
        """ Check encoding training_data """
        enc_error = check_encoding(training_csv)
        if enc_error != 'utf-8':
            logger.info('Check the encoding of the training_data, it must be utf-8.')
            sys.exit()
        else:
            logger.info('Encoding Training_data: ' + enc_error)

        """ Check encoding topics_csv """
        enc_error = check_encoding(topics_csv)
        if enc_error != 'utf-8':
            logger.info('Check the encoding of the topics_data, it must be utf-8.')
            sys.exit()
        else:
            logger.info('Encoding Topics_Data: ' + enc_error)

        """ Check empty and columns training_data """
        empty_error = check_empty_column(training_csv)
        if empty_error == 'empty':
            logger.info('The training_data is empty. Please, fill up.')
            sys.exit()
        elif empty_error == 'not_query':
            logger.info('The training_data has not "Query" column. Please, check it. Has to be "Query".')
            sys.exit()
        elif empty_error == 'not_property':
            logger.info('The training_data has not "Property" column. Please, check it. Has to be "Property".')
            sys.exit()

        """ Print Arguments """
        logger.info('Language: ' + lang)
        if isinstance(training_csv, pd.DataFrame):
            logger.info('Training_data: (DataFrame).')
            logger.info('Topics_data: (DataFrame).')
        else:
            logger.info('Training_data: (CSV) ' + training_csv)
            logger.info('Topics_data: (CSV) ' + topics_csv)

        logger.info('Model_dir: ' + model_dir)
        try:
            if synonyms_json:
                logger.info('Synonyms_json: ' + synonyms_json)
            else:
                logger.info('Synonyms_json: ' + str(None))
        except TypeError:
            logger.info('Synonyms_json loaded externally')

        logger.info('Clean_topics: ' + str(clean_topics))

        if isinstance(mapped_topics, pd.DataFrame):
            logger.info("mapped_topics has been loaded as dataframe")
        elif mapped_topics is not None:
            logger.info('mapped_topics: ' + mapped_topics)
        else:
            logger.info('mapped_topics: ' + str(None))

        logger.info('query_expansion: ' + str(query_expansion))

        params = {
            'logger': logger,
            'training_csv': training_csv,
            'model_dir': model_dir,
            'lang': lang,
            'topics_csv': topics_csv,
            'synon_json': synonyms_json,
            'clean_topics': clean_topics,
            'mapped_topics': mapped_topics,
            'query_expansion': query_expansion,
            'language_EN': 'english',
            'language_DE': 'german',
            'language_FR': 'french',
            'language_ES': 'spanish'
        }

    elif mode[1] == 'PREDICT':
        """ Print Arguments """
        logger.info('Language: ' + lang)
        logger.info('Model_dir: ' + model_dir)
        logger.info('Port: ' + str(port))
        logger.info('Clean_topics: ' + str(clean_topics))

        if isinstance(mapped_topics, pd.DataFrame):
            logger.info("mapped_topics has been loaded as dataframe")
        elif mapped_topics is not None:
            logger.info('mapped_topics: ' + mapped_topics)
        else:
            logger.info('mapped_topics: ' + str(None))

        params = {
            'logger': logger,
            'model_dir': model_dir,
            'lang': lang,
            'port': port,
            'clean_topics': clean_topics,
            'mapped_topics': mapped_topics,
            'language_EN': 'english',
            'language_DE': 'german',
            'language_FR': 'french',
            'language_ES': 'spanish'
        }

    elif mode[1] == 'TEST':
        """ Check encoding test_data """
        enc_error = check_encoding(test_data)
        if enc_error != 'utf-8':
            logger.info('Check the encoding of the test_data, it must be utf-8.')
            sys.exit()
        else:
            logger.info('Encoding Test_data: ' + enc_error)

        """ Check encoding topics_csv """
        if topics_csv is not None:
            enc_error = check_encoding(topics_csv)
            if enc_error != 'utf-8':
                logger.info('Check the encoding of the topics_data, it must be utf-8.')
                sys.exit()
            else:
                logger.info('Encoding Topics_Data: ' + enc_error)

        ''' Get none lang (Accuracy Test only) '''
        try:
            lang
        except NameError:
            lang = None

        ''' Get none url_service (Conflict Test only) '''
        try:
            url_service
        except NameError:
            url_service = None

        ''' Get none url_service (Accuracy Test only) '''
        try:
            synonyms_json
        except NameError:
            synonyms_json = None

        """ Check empty and columns training_data """
        empty_error = check_empty_column(test_data)
        if empty_error == 'empty':
            logger.info('The Test_data is empty. Please, fill up.')
            sys.exit()
        elif empty_error == 'not_query':
            logger.info('The Test_data has not "Query" column. Please, check it. Has to be "Query".')
            sys.exit()
        elif empty_error == 'not_property':
            logger.info('The Test_data has not "Property" column. Please, check it. Has to be "Property".')
            sys.exit()

        """ Print Arguments """
        if isinstance(test_data, pd.DataFrame):
            logger.info('test_data has been loaded as Dataframe')
        else:
            logger.info('test_data: ' + test_data)

        logger.info('language: ' + str(lang))

        logger.info('model_dir: ' + model_dir)

        if isinstance(topics_csv, pd.DataFrame):
            logger.info('topics_data has been loaded as Dataframe')
        else:
            logger.info('topics_data: ' + str(topics_csv))

        logger.info('multithreading_mode: %s' %multithreading)

        logger.info('target url: %s' %str(url_service))

        if isinstance(synonyms_json, list):
            logger.info('synonyms_json has been loaded has list')
        else:
            logger.info('synonyms_json: %s' %str(synonyms_json))

        params = {
            'logger': logger,
            'test_data': test_data,
            'model_dir': model_dir,
            'lang': lang,
            'topics_csv': topics_csv,
            'clean_topics': clean_topics,
            'multithreading': multithreading,
            'url_service': url_service,
            'synonyms_json': synonyms_json,
            'language_EN': 'english',
            'language_DE': 'german',
            'language_FR': 'french',
            'language_ES': 'spanish'
        }

    model_config = {
        'cosine': True,
        'spell_checker': True,
        'save_files': True
    }

    return params, model_config
