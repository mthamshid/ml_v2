import string
import re
import os
import json
import nltk
import sys
import spacy

from dateutil.parser import parse
from nltk.tokenize.moses import MosesDetokenizer
from nltk.corpus import wordnet
from spacy.tokens import Span
from spacy.matcher import Matcher
from ..Utils.data_handler import *

import pandas as pd


class Normalizer:
    def __init__(self, topics_data_df, conf_lang, synon_json=None, logger=None, lang='en', model_dir=None):
        # Load mandatory parameters
        self.topics_data_df = topics_data_df
        self.conf_lang = conf_lang
        self.synon_json = synon_json
        self.logger = logger
        self.lang = lang
        self.model_dir = model_dir

        # Load Synonyms file-json
        synon_to_word = synonyms_loader(synon_file=self.synon_json)

        # Read morphosemantics from Wordnet
        self.orthographics, self.arg2_sensekey = morphosemantics_loader(lang=self.lang, logger=self.logger)

        # Load model
        self.nlp = spacy.load(self.lang, disable=['parser', 'ner'])

        # Load contractions
        self.contractions = load_contractions_file()

        # Load language configuration
        features_lang = []
        for items in self.conf_lang:
            features_lang.append(items)
        self.language = self.conf_lang.get(features_lang[0])

        # Load stop punctuations
        self.stop_punctuations = '[' + string.punctuation + '¿`¨´¡' + ']'
        self.stop_punctuations = self.stop_punctuations.replace("$", "")

        # Create detokenizer objetct
        self.detokenizer = MosesDetokenizer()

        '''Matcher Stop Words'''
        self.matcher_words = Matcher(self.nlp.vocab)

        if self.language == 'english':
            stop_words = set(nltk.corpus.stopwords.words(self.language) + ['whether'])
        else:
            stop_words = set(nltk.corpus.stopwords.words(self.language))

        self.matcher_words = matcher_settings(nlp=self.nlp,
                                              matcher_object=self.matcher_words,
                                              file_to_match=stop_words,
                                              logger=self.logger)

        '''Matcher Stop Phrases'''
        self.matcher_phrases = Matcher(self.nlp.vocab)
        stop_phrases = stop_phrases_loader()
        self.matcher_phrases = matcher_settings(nlp=self.nlp,
                                                matcher_object=self.matcher_phrases,
                                                file_to_match=stop_phrases,
                                                logger=self.logger)

        ''' queries that start by any of this phrases '''
        self.matcher_iniphrases = Matcher(self.nlp.vocab)
        self.matcher_iniphrases = matcher_settings(nlp=self.nlp,
                                                   matcher_object=self.matcher_iniphrases,
                                                   file_to_match={'need', 'want', 'like'},
                                                   logger=self.logger)

        '''Matcher Topics'''
        topic2variant = create_toopictovariant(topics_df=self.topics_data_df,
                                               logger=self.logger)

        self.matcher_topics = Matcher(self.nlp.vocab)
        self.matcher_topics = matcher_settings(nlp=self.nlp,
                                               matcher_object=self.matcher_topics,
                                               file_to_match=topic2variant,
                                               logger=self.logger)

        '''Matcher Synonyms'''
        self.matcher_synonym = Matcher(self.nlp.vocab)
        self.matcher_synonym = matcher_settings(nlp=self.nlp,
                                                matcher_object=self.matcher_synonym,
                                                file_to_match=synon_to_word,
                                                logger=self.logger)

        '''Lemmatize topics'''
        for dict_topic in topic2variant.keys():
            current_list_of_topics = list(topic2variant[dict_topic])
            current_list_of_topics = [x.lower() for x in current_list_of_topics]
            iter_topics_list = []
            # Topic expansion
            for item in current_list_of_topics:
                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=False, cleanStopWords=False,
                                                                              cleanStopPhrases=False, lemmatize=True,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=False, cleanStopWords=False,
                                                                              cleanStopPhrases=True, lemmatize=True,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=False, cleanStopWords=True,
                                                                              cleanStopPhrases=True, lemmatize=True,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=True, cleanStopWords=True,
                                                                              cleanStopPhrases=True, lemmatize=True,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=False, cleanStopWords=False,
                                                                              cleanStopPhrases=False, lemmatize=False,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=True, cleanStopWords=False,
                                                                              cleanStopPhrases=False, lemmatize=False,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=True, cleanStopWords=True,
                                                                              cleanStopPhrases=False, lemmatize=False,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

                iter_topics_list.append(re.sub(' +', ' ', self.normalize(item, cleanPoints=True, cleanStopWords=True,
                                                                              cleanStopPhrases=True, lemmatize=False,
                                                                              lower_case=True, cleanTopics=False,
                                                                              normalizeSynonyms=False,
                                                                              detectTopics=False)[0]))

            topic2variant[dict_topic] = list(set(current_list_of_topics + iter_topics_list))

        '''Lemmatize Synonyms'''
        for dict_topic in synon_to_word.keys():
            current_list_of_topics = list(synon_to_word[dict_topic])
            current_list_of_topics = [x.lower() for x in current_list_of_topics]

            if not current_list_of_topics:
                current_list_of_topics.append(dict_topic)
            else:
                pass

            current_topic_lemma = None
            for item in current_list_of_topics:
                current_topic_lemma = []
                doc = self.nlp(item)
                for tok in doc:
                    if '-PRON-' in tok.lemma_:
                        current_topic_lemma.append(tok.text)
                    else:
                        current_topic_lemma.append(tok.lemma_)
                current_topic_lemma = self.detokenizer.detokenize(current_topic_lemma, return_str=True)
            current_list_of_topics.append(current_topic_lemma.lower())
            synon_to_word[dict_topic] = list(set(current_list_of_topics))

        # Store synon_to_word and topic2variant
        save_json_file(model_dir=self.model_dir,
                       file_to_save=topic2variant,
                       name_file='lemmatize_topics',
                       logger=self.logger)
        save_json_file(model_dir=self.model_dir,
                       file_to_save=synon_to_word,
                       name_file='lemmatize_synonyms',
                       logger=self.logger)

        self.synonyms = synon_to_word
        self.topics = topic2variant

        '''Matcher Topics Expanded'''
        self.matcher_topics = matcher_settings(nlp=self.nlp,
                                               matcher_object=self.matcher_topics,
                                               file_to_match=topic2variant,
                                               logger=self.logger)

        '''Matcher Synonyms Expanded'''
        self.matcher_synonym = matcher_settings(nlp=self.nlp,
                                                matcher_object=self.matcher_synonym,
                                                file_to_match=synon_to_word,
                                                logger=self.logger)

    @staticmethod
    def normalizeSynonims(query, matcher, nlp):

        matcherOffsets = []
        matcherOffsetsSet = set()  # remove duplicates
        ruleIds = {}
        doc = nlp(query)
        for match_id, start, end in matcher(doc):
            rule_id = nlp.vocab.strings[match_id]
            if (start, end) not in matcherOffsetsSet:
                matcherOffsets.append((start, end))
                matcherOffsetsSet.add((start, end))
            ruleIds[(start, end)] = rule_id

        # delete overlapping test spans

        toDelete = set()

        def overlaps(a, b):
            if (b[0] >= a[0] and b[0] <= a[1] - 1) or (b[1] - 1 >= a[0] and b[1] - 1 <= a[1] - 1):
                return True
            elif (a[0] >= b[0] and a[0] <= b[1] - 1) or (a[1] - 1 >= b[0] and a[1] - 1 <= b[1] - 1):
                return True
            else:
                return False

        def bigger(a, b):
            if a[1] - a[0] > b[1] - b[0]:
                return True
            else:
                return False

        for i in range(len(matcherOffsets)):
            if i + 1 < len(matcherOffsets):
                for j in range(i + 1, len(matcherOffsets)):
                    a = matcherOffsets[i]
                    b = matcherOffsets[j]
                    if a not in toDelete and b not in toDelete:
                        if overlaps(a, b):
                            if bigger(a, b):
                                toDelete.add(b)
                            else:
                                toDelete.add(a)

        matcherOffsets_clean = []
        for i in range(len(matcherOffsets)):
            if matcherOffsets[i] not in toDelete:
                matcherOffsets_clean.append(matcherOffsets[i])


        charOffsets = {}
        for offsets in matcherOffsets_clean:
            entity = Span(doc, offsets[0], offsets[1])
            charOffsets[(entity.start_char, entity.end_char)] = ruleIds[offsets]

        index_array = []
        for offsets in charOffsets:
            index_array.append(offsets[0])
            index_array.append(offsets[1])

        index_array_expanded = []
        if (len(index_array) > 0):
            index_array_expanded.append(index_array[0])

        for index in index_array[1::2]:
            start = index
            index_array_expanded.append(start - 1)
            if start != index_array[-1]:
                index_array_expanded.append(start)
            try:
                end = index_array[index_array.index(index) + 1]
                index_array_expanded.append(end - 1)
                index_array_expanded.append(end)
            except IndexError:
                pass

        if len(index_array_expanded) > 0 and index_array_expanded[-1] != len(query):
            index_array_expanded.append(index_array_expanded[-1] + 1)
            index_array_expanded.append(len(query))
        if len(index_array_expanded) > 0 and index_array_expanded[0] != 0:
            index_array_expanded.insert(0, 0)
            index_array_expanded.insert(1, index_array[0] - 1)

        string_array = []
        normalizedQuery = ''
        for index in index_array_expanded[0::2]:
            start = index
            end = index_array_expanded[index_array_expanded.index(index) + 1]
            offset = (start, end + 1)
            if offset in charOffsets:
                string_array.append(charOffsets[offset])
                normalizedQuery = normalizedQuery + charOffsets[offset]
            else:
                string_array.append(query[start:end + 1])
                normalizedQuery = normalizedQuery + query[start:end + 1]

        if len(normalizedQuery) == 0:
            normalizedQuery = query

        return normalizedQuery

    @staticmethod
    def clean_german_queries(query_csv):
        clean_query_csv = query_csv

        string = query_csv

        if 'ä' or 'Ä' in string:
            for ch in ['ä']:
                if ch in string:
                    string = string.replace(ch, "ae")
                    clean_query_csv = string
            for ch in ['Ä']:
                if ch in string:
                    string = string.replace(ch, "Ae")
                    clean_query_csv = string

        if 'ö' or 'Ö' in string:
            for ch in ['ö']:
                if ch in string:
                    string = string.replace(ch, "oe")
                    clean_query_csv = string

            for ch in ['Ö']:
                if ch in string:
                    string = string.replace(ch, "Oe")
                    clean_query_csv = string

        if 'ü' or 'Ü' in string:
            for ch in ['ü']:
                if ch in string:
                    string = string.replace(ch, "ue")
                    clean_query_csv = string

            for ch in ['Ü']:
                if ch in string:
                    string = string.replace(ch, "Ue")
                    clean_query_csv = string

        return clean_query_csv

    @staticmethod
    def negation_normalizer(query, lower_case, nlp):
        if lower_case:
            return nlp(query.replace("n't", 'noot').lower())
        else:
            return nlp(query.replace("n't", 'noot'))

    @staticmethod
    def contraction_normalizer(doc_object, contractions_file):
        tokens = []

        for tok in doc_object:
            if "'" in tok.text and tok.text not in contractions_file.keys():
                word_ = tok.text.split("'")
                for item in range(len(word_)):
                    if item % 2 != 0:
                        tokens.append(" '")
                        tokens.append(word_[item])
                    else:
                        tokens.append(word_[item])
            else:
                tokens.append(tok.text)

        return tokens

    @staticmethod
    def delete_by_matcher(matcher_object, boolean_condition, delete_set, doc_object, iniMatcher=False):
        if boolean_condition:
            matches = matcher_object(doc_object)
            for match in matches:
                if not iniMatcher or match[1] == 0:
                    for i in range(match[1], match[2]):
                        delete_set.add(i)

            return delete_set
        else:
            return delete_set

    @staticmethod
    def topics_inside_topics(dict_object):
        # Detect topics inside of topics
        i = 0
        topics_detected_final = []
        range_topic = []

        for key, value in dict_object.items():
            if i == 0:
                topics_detected_final.append(key)
                range_topic.append(value)
            else:
                rg_inter = []
                for rg in range_topic:
                    if rg[0] < value[0] < rg[1]:
                        rg_inter.append(True)
                if True in rg_inter:
                    pass
                else:
                    topics_detected_final.append(key)
                    range_topic.append(value)
            i += 1

        return topics_detected_final

    def topic_detector(self, matcher_object, doc_object, nlp):
        topics_detected = []
        words_topics_detected = []
        topictorange = {}

        for match_id, start, end in matcher_object(doc_object):
            rule_id = nlp.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
            topics_detected.append(rule_id)
            topictorange[rule_id] = [start, end]
            words_topics_detected.append(doc_object[start: end].text)

        topics_detected = self.topics_inside_topics(dict_object=topictorange)

        if len(topics_detected) == 0:
            topics_detected.append(None)
        if len(words_topics_detected) == 0:
            words_topics_detected.append(None)

        return topics_detected, words_topics_detected

    @staticmethod
    def tokens_deleter(delete_object, doc_object):
        tokens = []

        for tok in doc_object:
            # If we detect some topics delete them and return in a the list of topics detected
            if tok.i not in delete_object:
                tokens.append(tok.text)
            else:
                pass

        for token in range(len(tokens)):
            # End the normalization after check negation_normalizer
            if 'noot' in tokens[token]:
                tokens[token] = tokens[token].replace("noot", "n't")
            else:
                pass

        return tokens

    @staticmethod
    def ticket_cleaner(query):
        m = re.search(r'INC\d+', query, re.IGNORECASE)
        if m:
            query = query.replace(m.group(), 'INC0')
        m = re.search(r'RITM\d+', query, re.IGNORECASE)
        if m:
            query = query.replace(m.group(), 'RITM0')
        m = re.search(r'REQ\d+', query, re.IGNORECASE)
        if m:
            query = query.replace(m.group(), 'REQ0')

        return query

    @staticmethod
    def gerund_lemmatizer(token, lemmas_list):
        flag = False
        if token.text.endswith("ing"):
            lemma = token.text
            if lemma.endswith('ing'):
                # If token ends with ing we don't make morphosemantics
                flag = True
                lemma = lemma[:-3]
                if len(lemma) >= 2 and lemma[len(lemma) - 1] == lemma[len(lemma) - 2]:
                    lemma = lemma[:-1]
                else:
                    pass
            else:
                pass

            # Check if the lemma is present in Wordnet as a verb.
            if len(wordnet.synsets(lemma, 'v')) != 0:
                lemmas_list.append(lemma)

            # Else check if lemma+'e' is present in Wordnet as a verb.
            elif len(wordnet.synsets(lemma + 'e', 'v')) > 0:
                lemmas_list.append(lemma + 'e')

            # If nothing is found, don't do this lemmatizer
            else:
                lemmas_list.append(token.text)

        return lemmas_list, flag

    @staticmethod
    def alphanumeric_lemmatizer(token, lemmas_list):
        if token.text.isalnum() and not token.text.isalpha() and not token.text.isdigit():
            new_tokens = re.findall(r"[^\W\d_]+|\d+", token.text)
            for tok in new_tokens:
                lemmas_list.append(tok)

            return lemmas_list
        else:
            return lemmas_list

    @staticmethod
    def morphosemantics_lemmatizer(token, lemmas_list, arg2_sensekey_object, orthographics_object):
        if token.lemma_ in arg2_sensekey_object:
            lemmas_list.append(orthographics_object[arg2_sensekey_object.index(token.lemma_)]['arg1 sensekey'])
            return lemmas_list

        elif 'PRON' in token.lemma_:
            lemmas_list.append(token.text)
            return lemmas_list

        else:
            lemmas_list.append(token.lemma_)
            return lemmas_list

    def main_lemmatizer(self, nlp, query, language, arg2_sensekey_object, orthographics_object):
        doc = nlp(query)
        lemmas = []
        for token in doc:
            if language == 'en':
                """ Special rule for words like scanning. Force strip 'ing' for Noun cases"""

                lemmas, flag = self.gerund_lemmatizer(token=token,
                                                lemmas_list=lemmas)

                """We're only made morphosemantics lemmatizer if the current lemma it's not gerund"""
                if flag is True:
                    pass
                else:
                    """Special lemmatizer based on morphosemantics-Wordnet"""
                    lemmas = self.morphosemantics_lemmatizer(token=token,
                                                             lemmas_list=lemmas,
                                                             arg2_sensekey_object=arg2_sensekey_object,
                                                             orthographics_object=orthographics_object)
            else:
                pass

            """ Special rule for tokenizing. Like IB1545"""
            lemmas = self.alphanumeric_lemmatizer(token=token,
                                                  lemmas_list=lemmas)

        return self.detokenizer.detokenize(lemmas, return_str=True)

    @staticmethod
    def text2int(textnum, numwords={}):
        if not numwords:
            units = [
                "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
                "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
                "sixteen", "seventeen", "eighteen", "nineteen",
            ]

            tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

            scales = ["hundred", "thousand", "million", "billion", "trillion"]

            numwords["and"] = (1, 0)
            for idx, word in enumerate(units):
                numwords[word] = (1, idx)
            for idx, word in enumerate(tens):
                numwords[word] = (1, idx * 10)
            for idx, word in enumerate(scales):
                numwords[word] = (10 ** (idx * 3 or 2), 0)
        else:
            pass

        ordinal_words = {'first': 1, 'second': 2, 'third': 3, 'fifth': 5, 'eighth': 8, 'ninth': 9, 'twelfth': 12}
        ordinal_endings = [('ieth', 'y'), ('th', '')]
        textnum = textnum.replace('-', ' ')
        current = result = 0
        curstring = ""
        onnumber = False

        splited_textnum = textnum.split()
        if splited_textnum == ['and'] and len(splited_textnum) == 1:
            onnumber = False
        else:
            i = 0
            for word in splited_textnum:
                if word in ordinal_words:
                    scale, increment = (1, ordinal_words[word])
                    current = current * scale + increment
                    if scale > 100:
                        result += current
                        current = 0
                    onnumber = True
                else:
                    try:
                        next_ = splited_textnum[i + 1]
                    except IndexError:
                        next_ = ''

                    for ending, replacement in ordinal_endings:
                        if word.endswith(ending) and re.match('[0-9].*', word):
                            word = "%s%s" % (word[:-len(ending)], replacement)

                    if word not in numwords:
                        if onnumber:
                            curstring += repr(result + current) + " "
                        curstring += word + " "
                        result = current = 0
                        onnumber = False

                    elif word in numwords and next_ not in numwords:
                        if onnumber:
                            curstring += repr(result + current) + " "
                        curstring += word + " "
                        result = current = 0
                        onnumber = False

                    else:
                        scale, increment = numwords[word]
                        current = current * scale + increment
                        if scale > 100:
                            result += current
                            current = 0
                        onnumber = True
                i += 1

        if onnumber:
            curstring += repr(result + current)

        return curstring

    @staticmethod
    def normalize_date(query, nlp):

        found_list = []
        normalized_query = query
        doc = nlp(query)
        if len(doc.ents) == 0: return query
        for each in doc.ents:
            if each.label_ == "DATE":
                try:
                    converted_date = parse(each.text)  # .strftime("%Y%m%d")
                    normalized_query = query.replace(each.text, converted_date)
                    return normalized_query
                except Exception:
                    return query
            else:
                return query

    def normalize(self, data, cleanPoints=True, cleanStopWords=False, cleanStopPhrases=False, lemmatize=False,
                  lower_case=True, cleanTopics=False, normalizeSynonyms=True, detectTopics=True):

        cleaned_queries = []
        words_topics_detected = []
        topics_detected = []
        properties = None
        df = False

        if isinstance(data, pd.DataFrame):
            queries = data.Query.astype(str)
            properties = data.Property.astype(str)
            df = True
        else:
            queries = [data]
        # print('Original Query --> ', queries)

        # Normalize numbers
        converted_queries = []
        for each_queries in queries:
            converted_queries.append(self.text2int(textnum=each_queries))
        queries = converted_queries

        # print('After normalize numbers --> ', queries)
        # Normalize dates
        # date_ = []
        # for each_ in queries:
        #     date_.append(self.normalize_date(each_, nlp=self.nlp))
        # queries = date_

        if normalizeSynonyms:
            # Dictionary synonym normalizer
            normalized_query = []
            for query in queries:
                normalized_query.append(self.normalizeSynonims(query=query, matcher=self.matcher_synonym, nlp=self.nlp))
            queries = pd.Series(normalized_query)

            # print('After normalize SYNONYMS --> ',queries)

            # Topic synonym normalizer
            normalized_query = []
            for query in queries:
                normalized_query.append(self.normalizeSynonims(query=query, matcher=self.matcher_topics, nlp=self.nlp))
            queries = pd.Series(normalized_query)

            # print('After normalize TOPICS --> ', queries)
        else:
            pass

        for query in queries:
            doc = self.negation_normalizer(query=query,
                                           lower_case=lower_case,
                                           nlp=self.nlp)

            tokens_list = self.contraction_normalizer(doc_object=doc,
                                                      contractions_file=self.contractions)

            query = self.detokenizer.detokenize(tokens_list, return_str=True)

            # print('After negation and contraction normalizer --> ', query)

            doc = self.nlp(query)

            delete = set()
            # Delete topics
            delete = self.delete_by_matcher(matcher_object=self.matcher_topics,
                                            boolean_condition=cleanTopics,
                                            delete_set=delete,
                                            doc_object=doc)
            # Delete Stop Phrases
            delete = self.delete_by_matcher(matcher_object=self.matcher_phrases,
                                            boolean_condition=cleanStopPhrases,
                                            delete_set=delete,
                                            doc_object=doc,
                                            iniMatcher=True)

            delete = self.delete_by_matcher(matcher_object=self.matcher_phrases,
                                            boolean_condition=cleanStopPhrases,
                                            delete_set=delete,
                                            doc_object=doc,
                                            iniMatcher=False)
            # Delete Stop Words
            delete = self.delete_by_matcher(matcher_object=self.matcher_words,
                                            boolean_condition=cleanStopWords,
                                            delete_set=delete,
                                            doc_object=doc)

            # Detect topics before clean the query
            if detectTopics:
                topics_detected, words_topics_detected = self.topic_detector(matcher_object=self.matcher_topics,
                                                                             doc_object=doc,
                                                                             nlp=self.nlp)
            else:
                topics_detected.append(None)
                words_topics_detected.append(None)

            cleaned_tokens = self.tokens_deleter(delete_object=delete,
                                                 doc_object=doc)

            query = self.detokenizer.detokenize(cleaned_tokens, return_str=True)

            # print('After clean topics, stop words and utterances --> ', query)

            if lemmatize:
                query = self.main_lemmatizer(nlp=self.nlp,
                                             query=query,
                                             language=self.lang,
                                             arg2_sensekey_object=self.arg2_sensekey,
                                             orthographics_object=self.orthographics)

                # print('After lemmatize --> ', query)

                doc = self.nlp(query)
                delete = set()

                delete = self.delete_by_matcher(matcher_object=self.matcher_topics,
                                                boolean_condition=cleanTopics,
                                                delete_set=delete,
                                                doc_object=doc)

                # Detect topics before clean the query
                if detectTopics:
                    topics_detected, words_topics_detected = self.topic_detector(matcher_object=self.matcher_topics,
                                                                                 doc_object=doc,
                                                                                 nlp=self.nlp)
                else:
                    words_topics_detected.append(None)
                    topics_detected.append(None)

                cleaned_tokens = self.tokens_deleter(delete_object=delete,
                                                     doc_object=doc)

                query = self.detokenizer.detokenize(cleaned_tokens, return_str=True)
                # print('After clean topics 2 --> ', query)

            else:
                # End if Lemmatize
                pass

            if self.lang == "de":
                query = self.clean_german_queries(query)

            if cleanPoints:
                query = re.sub(str(self.stop_punctuations), ' ', query)

            # print('After clean Points --> ', query)

            # Clean tickets like RITM, REQ...
            query = self.ticket_cleaner(query=query)

            cleaned_queries.append(query.strip())

            # print('---- OUTPUT QUERY -----> ', cleaned_queries, '\n')

        if df:
            cleaned_dataset = pd.DataFrame(
                {'Property': properties,
                 'Query': cleaned_queries,
                 })
            return cleaned_dataset
        elif detectTopics is True:
            return cleaned_queries, topics_detected, words_topics_detected
        else:
            return cleaned_queries