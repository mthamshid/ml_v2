import json
import os
import sys
import spacy
import pandas as pd

from io import StringIO
from spacy.matcher import Matcher


def read_csv(training_csv, empty_bytes=False):
    """
    This function returns the dataframe
    :param:  - file_path or dataframe
    :return: - dataframe
    """
    # ---------- OLD read_csv FUNCTION ---------------
    # if isinstance(training_csv, pd.DataFrame):
    #     training_df = training_csv
    #     training_df = delete_unnamed_col(df=training_df)
    #     training_df = check_column_names(training_df)
    #     return training_df
    # else:
    #     training_df = pd.read_csv(training_csv, encoding='utf-8')
    #     training_df = training_df.loc[:, ~training_df.columns.str.contains('^Unnamed')]
    #     training_df = check_column_names(training_df)
    #     return training_df

    # Delete empty bytes
    # if empty_bytes:
    #     training_df = del_empty_bytes(training_df, columns=['Query'])
    # -------------------------------------------------

    training_csv = check_sep(file=training_csv)
    return check_column_names(training_csv)


def check_sep(file):
    if isinstance(file, pd.DataFrame):
        df_param = file
    else:
        # 1st .- Read dataframe
        df_param = pd.read_csv(file, encoding='utf-8')

    # 2nd .- Check length of columns
    if len(df_param.columns) <= 1:

        # 3rd .- Try with the correct delimiter
        if len(df_param.columns[0].split(';')) > 1:
            return delete_unnamed_col(df=pd.read_csv(file, sep=';', encoding='utf-8'))

        elif len(df_param.columns[0].split(',')) > 1:
            return delete_unnamed_col(df=pd.read_csv(file, sep=',', encoding='utf-8'))

        elif len(df_param.columns[0].split('.')) > 1:
            return delete_unnamed_col(df=pd.read_csv(file, sep='.', encoding='utf-8'))

        elif len(df_param.columns[0].split('\t')) > 1:
            return delete_unnamed_col(df=pd.read_csv(file, sep='\t', encoding='utf-8'))

        elif len(df_param.columns[0].split('|')) > 1:
            return delete_unnamed_col(df=pd.read_csv(file, sep='|', encoding='utf-8'))
        else:
            return delete_unnamed_col(df=df_param)
    else:
        return delete_unnamed_col(df=df_param)


def delete_unnamed_col(df):
    searchfor = ['Unnamed', 'unnamed', 'UNNAMED',
                 'Unamed', 'unamed', 'UNAMED']
    lower_cols = [x.lower() for x in df.columns]
    if any("unnamed" in s for s in lower_cols) or any("unamed" in s for s in lower_cols):
        return df.loc[:, ~df.columns.str.contains('|'.join(searchfor))]
    else:
        return df


def csv_to_list(training_csv):
    """
    This function extracts query and property from dataframe
    :param  - dataframe
    :return - query_list
            - query_property
    """
    # 1st read the csv file as pandas and check the name of the columns
    training_dataframe = read_csv(training_csv)

    data_query = training_dataframe.Query.tolist()
    data_property = training_dataframe.Property.tolist()
    return data_query, data_property


def save_csv(training_dataframe, model_dir, name_file):
    """
    This function saves the csv
    :param - dataframe
           - model directory
           - file name
    :return
    """
    training_dataframe.to_csv(os.path.join(model_dir, name_file + '.csv'), sep=',', encoding='utf-8', index=False)
    return


def individual_columns_csv(training_dataframe, column):
    """
    This function extracts specific columns
    :param  - dataframe
            - column names
    :return - columns
    """
    return training_dataframe.iloc[:, column].values.tolist()


def csv_to_pandas(arg1, arg2):
    """
    This function create a dataframe
    :param  - data
            - columns
    :return - dataframe
    """
    data = []
    for i in range(len(arg2[0])):
        item = []
        for k in range(len(arg2)):
            item.append(arg2[k][i])
        data.append(item)

    df = pd.DataFrame(data, columns=arg1)
    return df


def check_encoding(training_dataframe):
    """
    This function checks the encoding of the dataframe
    :param  - dataframe
    :return - string
    """
    try:
        read_csv(training_dataframe)
        return 'utf-8'
    except UnicodeDecodeError:
        return 'Check the encoding of the training_data, it must be utf-8.'


def del_empty_bytes(training_dataframe, columns):
    for col in columns:
        training_dataframe[col] = training_dataframe[col].str.replace('[^\x00-\x7F]', ' ')

    return training_dataframe


def check_column_names(training_dataframe):
    """
    This function checks the column names
    :param  - dataframe
    :return - dataframe (with new column names)
    """
    columns = list(training_dataframe.columns)
    new_name_columns = []
    for idx in range(len(columns)):
        col = columns[idx]

        '''PROPERTY COLUMN'''
        if col.lower() == 'property'.lower():
            new_name_columns.append('Property')
        elif col.lower() == 'properties'.lower():
            new_name_columns.append('Property')
        elif col.lower() == 'intent'.lower():
            new_name_columns.append('Property')
        elif col.lower() == 'intention'.lower():
            new_name_columns.append('Property')
        elif col.lower() == 'intentions'.lower():
            new_name_columns.append('Property')

        elif col.lower() == 'query'.lower():
            '''QUERY COLUMN'''
            new_name_columns.append('Query')
        elif col.lower() == 'querys'.lower():
            new_name_columns.append('Query')
        elif col.lower() == 'queries'.lower():
            new_name_columns.append('Query')
        elif col.lower() == 'utterance'.lower():
            new_name_columns.append('Query')
        elif col.lower() == 'utterances'.lower():
            new_name_columns.append('Query')
        elif col.lower() == 'variation'.lower() and 'TOPIC' in columns:
            new_name_columns.append('VARIATION')
        elif col.lower() == 'variation'.lower() and 'TOPIC' not in columns:
            new_name_columns.append('Query')
        elif col.lower() == 'variations'.lower() and 'TOPIC' in columns:
            new_name_columns.append('VARIATION')
        elif col.lower() == 'variations'.lower() and 'TOPIC' not in columns:
            new_name_columns.append('Query')
        else:
            new_name_columns.append(col)

        # Old version
        # if col.lower().startswith('propert') or col.lower().startswith('intent'):
        #     new_name_columns.append('Property')
        # elif col.lower().startswith('quer') or col.lower().startswith('uttera') or col.lower().startswith('utera') \
        #         or col.lower().startswith('feature'):
        #     new_name_columns.append('Query')
        # else:
        #     new_name_columns.append(col)

    training_dataframe.columns = new_name_columns
    return training_dataframe


def check_empty_column(training_dataframe):
    """
    This function checks empty columns
    :param  - dataframe
    :return - string
    """
    training_dataframe = read_csv(training_dataframe)

    if training_dataframe.empty:
        return 'empty'
    if 'Query' not in training_dataframe.columns:
        return 'not_query'
    if 'Property' not in training_dataframe.columns:
        return 'not_property'


def get_name_of_file(file_path):
    """
    This function returns the name of a file without extension given a path
    :param  - file_path
    :return - string --> name of the filename
    """
    return os.path.splitext(os.path.basename(file_path))[0]


def save_var(file_csv=None, topics_csv=None, synonyms_json=None, model_dir=None, mapped_topics=None):
    """
    This function saves
    :param  - var
            - dir_model
    """
    if isinstance(file_csv, pd.DataFrame):
        """Mandatory variables"""
        file_csv = check_column_names(training_dataframe=file_csv)
        file_csv['Query'] = file_csv['Query'].map(lambda x: x.encode('unicode-escape').decode('utf-8'))
        file_csv.to_csv(os.path.join(model_dir, 'file_csv.csv'))
        topics_csv.to_csv(os.path.join(model_dir, 'topic_file.csv'), encoding='utf-8')
        """Optional variables"""
        if mapped_topics is not None:
            mapped_topics.to_csv(os.path.join(model_dir, 'mapped_topics.csv'), encoding='utf-8')
        else:
            pass
    if synonyms_json is not None:
        with open(os.path.join(model_dir, 'synonyms_json_training.json'), 'w') as outfile:
            json.dump(synonyms_json, outfile)
    else:
        pass


def load_contractions_file():

    conc_file = {
          "ain't": "am not",
          "aren't": "are not",
          "can't": "cannot",
          "can't've": "cannot have",
          "'cause": "because",
          "could've": "could have",
          "couldn't": "could not",
          "couldn't've": "could not have",
          "didn't": "did not",
          "doesn't": "does not",
          "don't": "do not",
          "hadn't": "had not",
          "hadn't've": "had not have",
          "hasn't": "has not",
          "haven't": "have not",
          "he'd": "he would",
          "he'd've": "he would have",
          "he'll": "he will",
          "he'll've": "he will have",
          "he's": "he is",
          "how'd": "how did",
          "how'd'y": "how do you",
          "how'll": "how will",
          "how's": "how is",
          "i'd": "I would",
          "i'd've": "I would have",
          "i'll": "I will",
          "i'll've": "I will have",
          "i'm": "I am",
          "i've": "I have",
          "isn't": "is not",
          "it'd": "it had",
          "it'd've": "it would have",
          "it'll": "it will",
          "it'll've": "it will have",
          "it's": "it is",
          "let's": "let us",
          "ma'am": "madam",
          "mayn't": "may not",
          "might've": "might have",
          "mightn't": "might not",
          "mightn't've": "might not have",
          "must've": "must have",
          "mustn't": "must not",
          "mustn't've": "must not have",
          "needn't": "need not",
          "needn't've": "need not have",
          "o'clock": "of the clock",
          "oughtn't": "ought not",
          "oughtn't've": "ought not have",
          "shan't": "shall not",
          "sha'n't": "shall not",
          "shan't've": "shall not have",
          "she'd": "she would",
          "she'd've": "she would have",
          "she'll": "she will",
          "she'll've": "she will have",
          "she's": "she is",
          "should've": "should have",
          "shouldn't": "should not",
          "shouldn't've": "should not have",
          "so've": "so have",
          "so's": "so is",
          "that'd": "that would",
          "that'd've": "that would have",
          "that's": "that is",
          "there'd": "there had",
          "there'd've": "there would have",
          "there's": "there is",
          "they'd": "they would",
          "they'd've": "they would have",
          "they'll": "they will",
          "they'll've": "they will have",
          "they're": "they are",
          "they've": "they have",
          "to've": "to have",
          "wasn't": "was not",
          "we'd": "we had",
          "we'd've": "we would have",
          "we'll": "we will",
          "we'll've": "we will have",
          "we're": "we are",
          "we've": "we have",
          "weren't": "were not",
          "what'll": "what will",
          "what'll've": "what will have",
          "what're": "what are",
          "what's": "what is",
          "what've": "what have",
          "when's": "when is",
          "when've": "when have",
          "where'd": "where did",
          "where's": "where is",
          "where've": "where have",
          "who'll": "who will",
          "who'll've": "who will have",
          "who's": "who is",
          "who've": "who have",
          "why's": "why is",
          "why've": "why have",
          "will've": "will have",
          "won't": "will not",
          "won't've": "will not have",
          "would've": "would have",
          "wouldn't": "would not",
          "wouldn't've": "would not have",
          "y'all": "you all",
          "y'alls": "you alls",
          "y'all'd": "you all would",
          "y'all'd've": "you all would have",
          "y'all're": "you all are",
          "y'all've": "you all have",
          "you'd": "you had",
          "you'd've": "you would have",
          "you'll": "you you will",
          "you'll've": "you you will have",
          "you're": "you are",
          "you've": "you have"
        }
    return conc_file


def check_if_topic_lpl(list_of_topics):
    lpl_isa_topics = ['move money tool', 'Move Money Tool',
                      'Coverdell Educational Savings Account(ESA)',
                      'ClientWorks'
                      ]

    if isinstance(list_of_topics, pd.DataFrame):
        pass
    else:
        list_of_topics = read_csv(list_of_topics)

    try:
        ''' Get Topics column '''
        topics = list_of_topics.TOPIC.tolist()
        ''' Get Variations column '''
        variations = list_of_topics.VARIATION.tolist()
    except Exception as e:
        return 'Exception: error reading topics.csv file.'
        sys.exit()

    all_topics = topics + variations
    is_lpl = None

    for item in all_topics:
        if item in lpl_isa_topics:
            is_lpl = True
            break
        else:
            is_lpl = False

    return is_lpl


def load_lpl_feature_map():
    # Assign to z the csv
    z = """"""

    return pd.read_csv(StringIO(z), sep='\t')


def synonyms_loader(synon_file):
    # Load Synonyms file-json
    # Get words_to_syns
    synon_to_word = {}
    if synon_file is not None:
        for syns in synon_file:
            value = syns.get("value")
            syns_word = syns.get("synonyms")
            synon_to_word[value] = syns_word
    return synon_to_word


# # Load Synonyms file-json
# # Get words_to_syns
# synon_to_word = {}
# if synon_json is not None:
#     for syns in synon_json:
#         value = syns.get("value")
#         syns_word = syns.get("synonyms")
#         synon_to_word[value] = syns_word


def morphosemantics_loader(lang, logger):
    # Read morphosemantics from Wordnet
    if lang == 'en':
        morph_dir = ""
        try:
            for dirpath, dirnames, files in os.walk(os.path.abspath('../')):
                if 'morphosemantics' in dirpath.lower():
                    morph_dir = dirpath

            arg2_sensekey = []
            try:
                with open(os.path.join(morph_dir, 'morphosemantics_en.json')) as f:
                    morphosemantics = json.load(f)

                orthographics = morphosemantics["morphosemantics"]
                for items in orthographics:
                    arg2_sensekey.append(items["arg2 sensekey"])

                return orthographics, arg2_sensekey

            except FileNotFoundError:
                logger.info("\t---> morphosemantics_en.json is not found. Please, add it.")
                sys.exit()

        except Exception as e:
            logger.info('\t---> Please, check if you have the morphosemantics file for the required language %s' % lang)
            sys.exit()
    else:
        logger.info('\t---> Please, check if you have the morphosemantics files for the required language %s' % lang)
        sys.exit()


# # Read morphosemantics from Wordnet
# if self.lang == 'en':
#     morph_dir = ""
#     try:
#         for dirpath, dirnames, files in os.walk(os.path.abspath('../')):
#             if 'morphosemantics' in dirpath:
#                 morph_dir = dirpath
#
#         self.orthographics = []
#         self.arg2_sensekey = []
#         try:
#             with open(os.path.join(morph_dir, 'morphosemantics_en.json')) as f:
#                 morphosemantics = json.load(f)
#             self.orthographics = morphosemantics["morphosemantics"]
#             for items in self.orthographics:
#                 self.arg2_sensekey.append(items["arg2 sensekey"])
#         except FileNotFoundError:
#             self.logfile.info("\t---> morphosemantics_en.json is not found. Please, add it.")
#             sys.exit()
#
#     except Exception as e:
#         self.logfile.info(e)
#         self.logfile.info('\t---> Please, check if you have the morphosemantics files for the required language '
#                          + self.lang)
#         sys.exit()


def stop_phrases_loader():
    return [
        'i need', 'i need to', 'need to',
        'i want', 'i want to', 'want to', 'wanna', 'i wanna', 'do you want',
        'i would like', "i'd like", 'i would like to', "i'd like to",
        'can you', 'could you', 'will you', 'want to', 'want', 'do you have',
        'please', 'i am trying to', 'i try to',
        'can you help', 'help me', 'please', 'you help', 'can you help me',
        'do you mind', 'have you', 'do you have',
        'aid me', 'serve me', 'give me a hand', 'give a hand',
        'can you please', 'do you know', 'how do I', 'how can I', 'can I', 'how to', 'is it possible',
        'details about',
        'I need to', 'need to', 'I want to', 'want to', 'help to', 'I need help',
        'I have a query regarding', 'I have a query',
        'query regarding', 'I was wondering', 'I am wondering', "I'm wondering", 'I wonder',
        'I urgently need', 'I am inquiring about',
        'I am inquiring', "I'm inquiring",
        'I really need to', 'I really need', 'really need',
        "hi", "hello", "what's your favorite", 'your favorite',
        "i want to know about", "information about", "i want to know", "wanna know", "want to know",
        "information regarding", "tell me", "list me", "list out", "list", "explain me", "give me details",
        "give me details about", "i need to know", "need to know",
        "details regarding"]


def save_json_file(model_dir, file_to_save, name_file, logger):
    if model_dir is None:
        logger.info('\t ---> No folder to save model files provided !')
        pass
    else:
        with open(os.path.join(model_dir, str(name_file + '.json')), 'w') as outfile:
            json.dump(file_to_save, outfile)
        logger.info('\t --> File: %s' % name_file + '.json' + ' stored in: %s' % model_dir)


def matcher_settings(nlp, matcher_object, file_to_match, logger):
    if isinstance(matcher_object, spacy.matcher.Matcher):
        # Matcher for list
        if isinstance(file_to_match, list) or isinstance(file_to_match, set):
            for item in file_to_match:
                pattern = []
                doc = nlp(item)
                for tok in doc:
                    pattern.append({'LOWER': tok.lower_})
                if item.lower() not in matcher_object:
                    matcher_object.add(item.lower(), None, pattern)
            return matcher_object

        # Matcher for dict
        elif isinstance(file_to_match, dict):
            for key, value in file_to_match.items():
                file_to_match[key] = set(value)
                for variation in value:
                    # If variation is empty '' we don't lemmmatize it.
                    if not variation:
                        pass
                    else:
                        doc = nlp(variation)
                        tokens = []
                        pattern = []
                        for tok in doc:
                            tokens.append(tok.text)
                        for token in tokens:
                            dict_pattern = {'LOWER': token.lower()}
                            pattern.append(dict_pattern)
                        matcher_object.add(key, None, pattern)
            return matcher_object

        else:
            logger.info('\t ---> The file to create the matcher must be a list o dict')
    else:
        logger.info('\t ---> Bad matcher object selected')


# for stop_word in stop_words:
#     pattern = []
#     stop_doc = self.nlp(stop_word)
#     for tok in stop_doc:
#         pattern.append({'LOWER': tok.lower_})
#     if stop_word.lower() not in self.matcher_words:
#         self.matcher_words.add(stop_word.lower(), None, pattern)
#
# '''Matcher Topics'''
# self.matcher_topics = Matcher(self.nlp.vocab)
# for key, value in topic2variat.items():
#     topic2variat[key] = set(value)
#     for variation in value:
#         doc = self.nlp(variation)
#         tokens = []
#         pattern = []
#         for tok in doc:
#             tokens.append(tok.text)
#         for token in tokens:
#             dict_pattern = {'LOWER': token.lower()}
#             pattern.append(dict_pattern)
#         self.matcher_topics.add(key, None, pattern)


def create_toopictovariant(topics_df, logger):
    try:
        ''' Get Topics column '''
        topics = topics_df.TOPIC.tolist()
        ''' Get Variations column '''
        variations = topics_df.VARIATION.tolist()
    except Exception as e:
        logger.info(e)
        logger.info('\t ---> Exception: error reading topics.csv file.')
        sys.exit()

    topic2variant = {}
    for idx in range(len(topics)):
        topic = topics[idx]
        variat = variations[idx]
        if topic not in topic2variant:
            if str(variat) == 'nan':
                topic2variant[topic] = [topic]
            else:
                topic2variant[topic] = [variat]
        else:
            if str(variat) == 'nan':
                pass
            else:
                topic2variant[topic].append(variat)

    return topic2variant

# try:
#     ''' Get Topics column '''
#     topics = self.topics_data_df.TOPIC.tolist()
#     ''' Get Variations column '''
#     variations = self.topics_data_df.VARIATION.tolist()
# except Exception as e:
#     logger.info(e)
#     logger.info('Exception: error reading topics.csv file.')
#     sys.exit()
#
# topic2variat = {}
# for idx in range(len(topics)):
#     topic = topics[idx]
#     variat = variations[idx]
#     if topic not in topic2variat:
#         if str(variat) == 'nan':
#             topic2variat[topic] = [topic]
#         else:
#             topic2variat[topic] = [variat]
#     else:
#         if str(variat) == 'nan':
#             pass
#         else:
#             topic2variat[topic].append(variat)


def topics_mapping_dict(df_mappings):
    if df_mappings is not None:
        # If exits columns without name, delete them
        df_mappings = df_mappings.loc[:, ~df_mappings.columns.str.contains('^Unnamed')]

        # Make a topy of the original topic_mappings file
        no_property_mappings = df_mappings.copy()

        # Check what column use to make the dict, id or property display name
        if 'PropertyForDisplay' in df_mappings.columns and 'Property' in df_mappings.columns:
            no_property_mappings = no_property_mappings.drop(['Property'], 1)
            no_property_mappings = no_property_mappings.drop(['PropertyForDisplay'], 1)
            column_prop_df_mappings = 'Property'

        elif 'PropertyForDisplay' in df_mappings.columns and 'Property' not in df_mappings.columns:
            no_property_mappings = no_property_mappings.drop(['PropertyForDisplay'], 1)
            column_prop_df_mappings = 'PropertyForDisplay'

        elif 'PropertyForDisplay' not in df_mappings.columns and 'Property' in df_mappings.columns:
            no_property_mappings = no_property_mappings.drop(['Property'], 1)
            column_prop_df_mappings = 'Property'
        else:
            pass

        all_topics = []

        for property_mappings in range(len(df_mappings[column_prop_df_mappings])):
            row_list = []
            for name_column in no_property_mappings.iloc[[property_mappings]]:
                for index, row in no_property_mappings.iloc[[property_mappings]].iterrows():
                    try:
                        topics = json.loads(row[name_column])
                        for item in topics:
                            row_list.append(item)
                    except Exception as e:
                        row_list.append(row[name_column])
            all_topics.append(list(set(row_list)))

        return dict(zip(df_mappings[column_prop_df_mappings], all_topics))

    else:
        pass


def expand_dataset_by_topics(df_train, df_mappings):
    # If exits columns without name, delete them
    df_train = df_train.loc[:, ~df_train.columns.str.contains('^Unnamed')]
    df_mappings = df_mappings.loc[:, ~df_mappings.columns.str.contains('^Unnamed')]

    # First of all take a copy of the mapped topics without Property column
    no_property_mappings = df_mappings.copy()

    if 'PropertyForDisplay' in no_property_mappings.columns and 'Property' in no_property_mappings.columns:
        no_property_mappings = no_property_mappings.drop(['Property'], 1)
        no_property_mappings = no_property_mappings.drop(['PropertyForDisplay'], 1)
        column_prop_df_mappings = 'Property'

    elif 'PropertyForDisplay' in no_property_mappings.columns and 'Property' not in no_property_mappings.columns:
        no_property_mappings = no_property_mappings.drop(['PropertyForDisplay'], 1)
        column_prop_df_mappings = 'PropertyForDisplay'

    elif 'PropertyForDisplay' not in no_property_mappings.columns and 'Property' in no_property_mappings.columns:
        no_property_mappings = no_property_mappings.drop(['Property'], 1)
        column_prop_df_mappings = 'Property'
    else:
        pass

    # List for property for expanded querys
    final_property = []
    # List of expanded querys
    final_query = []
    # Set of index rows that appears in df_mappings
    mapped_items = set()

    for property_train in range(len(df_train['Property'])):
        for property_mappings in range(len(df_mappings[column_prop_df_mappings])):
            if df_train['Property'][property_train].lower() == df_mappings[column_prop_df_mappings][
                property_mappings].lower():
                if df_train['Property'][property_train] not in mapped_items:
                    mapped_items.add(property_train)
                else:
                    pass
                for name_column in no_property_mappings.iloc[[property_mappings]]:
                    for index, row in no_property_mappings.iloc[[property_mappings]].iterrows():
                        # str_to_list = re.split(r'[\,"\'\[\]] ?', row[name_column])
                        # topics = list(filter(None, str_to_list))
                        try:
                            topics = json.loads(row[name_column])
                        except Exception as e:
                            topics = []
                            topics.append(row[name_column])
                        for topic in topics:
                            if str(topic) == 'nan' or str(topic.lower()) in df_train['Query'][property_train].lower():
                                pass
                            else:
                                # First get the property to expand querys
                                final_property.append(df_train['Property'][property_train])
                                # Expand querys with their mapping topics
                                final_query.append(
                                    str(df_train['Query'][property_train].lower() + ' ' + topic.lower()))
            else:
                pass

    # Take the rows of the original training data that doesn't match with the mapped topics
    no_mapped_subset = df_train[~df_train.index.isin(list(mapped_items))]

    # Get a new training data
    new_querys = pd.DataFrame({
        'Property': final_property,
        'Query': final_query
    })

    training_data = pd.concat([no_mapped_subset, new_querys], ignore_index=True, keys=['Property', 'Query'])

    training_data_ = training_data.drop_duplicates(keep='first', inplace=False)
    # training_data_ = training_data_.reset_index()

    return training_data_
