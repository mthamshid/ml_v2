from spacy.matcher import Matcher


class SynonymsMatcher:
    def __init__(self, logger, nlp=None):
        self.logger = logger
        self.nlp = nlp

        self.matcher_syns = Matcher(self.nlp.vocab)

    def syn_matcher(self, synons):
        self.logger.info("\t---> Building Synonymous Matcher")

        self.matcher_syns = Matcher(self.nlp.vocab)
        for key, value in synons.items():
            synons[key] = set(value)
            for variation in value:
                doc = self.nlp(variation)
                tokens = []
                pattern = []
                for tok in doc:
                    tokens.append(tok.text)
                for token in tokens:
                    dict_pattern = {'LOWER': token.lower()}
                    pattern.append(dict_pattern)
                self.matcher_syns.add(key, None, pattern)

        self.logger.info("\t---> Synonymous Matcher Done")

        return self.matcher_syns
