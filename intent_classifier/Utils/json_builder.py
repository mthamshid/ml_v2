
class JsonBuilder:
    def __init__(self, logger, nlp, query_original, query_modified=None, query_spell_checker=None, debug_mode=None,
                 result_cosine=None, train_query_output=None, model_dir=None, list_of_topics=None,
                 list_words_topics=None,
                 queries_to_predict=None):

        self.nlp = nlp
        self.debug_mode = debug_mode
        self.result_cosine = result_cosine
        self.train_query_output = train_query_output
        self.query_modified = query_modified
        self.queries_to_predict = queries_to_predict
        self.list_of_topics = list_of_topics
        self.list_words_topics = list_words_topics

        self.query_original = query_original
        self.query_spell_checker = query_spell_checker

        self.logger = logger
        self.model_dir = model_dir

    @staticmethod
    def get_intent_by_index(result, idx=0):
        first_result_class = None
        first_result_prob = None

        try:
            first_result = result[idx]
            first_result_class = first_result.get("classification")
            first_result_prob = float(first_result.get("prob"))
        except IndexError:
            pass
        except AttributeError:
            pass

        return first_result_class, first_result_prob

    def intention_predicted(self, results_cosine):
        if not results_cosine:
            result = ['', float(0.00)]

        else:
            res_cos_1, prob_cos_1 = self.get_intent_by_index(results_cosine, 0)
            result = [res_cos_1, prob_cos_1]

        return result

    def get_result_cosine(self):
        result = []
        count = 0

        if float(self.result_cosine[0][1]) == 0.00 and str(self.debug_mode).lower() == 'true':
            result.append({'classification': 'Unknown_Class',
                           'prob': float(1.00),
                           "closest_utterance": ""})

        elif float(self.result_cosine[0][1]) == 0.00 and str(self.debug_mode).lower() != 'true':
            result.append({'classification': 'Unknown_Class',
                           'prob': float(1.00)})

        else:
            # Always get the Related Intents
            intention_set_debug = set()
            query_to_display = 0
            for (intention, sim) in self.result_cosine:
                if intention not in intention_set_debug and count < 20 and float(sim) > 0.01:
                    # if intention not in intention_set_debug and count < 20:
                    if str(self.debug_mode).lower() == 'true':
                        result.append({'classification': intention,
                                       'prob': float('%.2f' % sim),
                                       'closest_utterance': self.train_query_output[query_to_display][0]})

                    else:
                        result.append({'classification': intention, 'prob': float('%.2f' % sim)})
                    intention_set_debug.add(intention)
                    count += 1
                    query_to_display += 1
                else:
                    query_to_display += 1
                    pass

            self.logger.info("\t---> Result Cosine: " + str(result))
        return result

    def check_words_topic_sentence(self, list_words_topics, query_modified):
        """ Check if deleting topics, the sentence is empty to return Unknown_Class"""

        doc = self.nlp(query_modified)
        final_sent = []
        for tok in doc:
            if tok.text not in list_words_topics:
                final_sent.append(tok.text)

        # If sentence not empty
        if final_sent:
            return False
        # If sentence empty
        else:
            return True

    def build_json(self, query_original, query_modified, query_spell_checker, related_intents=None,
                   list_of_topics=None, check_words_topic=False, queries_to_predict=None):

        json_obj = dict()
        json_obj.update({'InputQuery': query_original})
        json_obj.update({'ModifiedQuery': query_modified})
        json_obj.update({'Modified Querys': self.queries_to_predict})
        json_obj.update({'Topics': list_of_topics})

        if check_words_topic and self.debug_mode == 'True':
            int_predicted = [{"classification": 'Unknown_Class',
                              "prob": float(0.9),
                              "closest_utterance": ''}]

        elif check_words_topic and self.debug_mode != 'False':
            int_predicted = [{"classification": 'Unknown_Class',
                              "prob": float(0.9)}]
        else:
            int_predicted = self.intention_predicted(related_intents)
            if str(self.debug_mode).lower() == 'true':
                int_predicted = [{"classification": int_predicted[0],
                                  "prob": float(int_predicted[1]),
                                  "closest_utterance": self.train_query_output[0][0]}]
            else:
                int_predicted = [{
                    "classification": int_predicted[0],
                    "prob": float(int_predicted[1])
                }]

        query_spell = query_spell_checker[0]
        query_expanded = query_spell_checker[1]

        spell_checker = [{'SpellCorrectedText': query_spell,
                          'SynonymQuery': query_expanded}]

        json_obj.update({'SpellCorrect': spell_checker})
        json_obj.update({'Intent': int_predicted})

        json_obj.update({'RelatedIntents': related_intents})

        return json_obj

    def builder(self):
        result_cosine_rules = self.get_result_cosine()
        check_words_topic = self.check_words_topic_sentence(self.list_words_topics, self.query_modified)
        json_obj = self.build_json(self.query_original, self.query_modified, self.query_spell_checker,
                                   result_cosine_rules, self.list_of_topics, check_words_topic)
        return json_obj


class JsonBoosted:
    def __init__(self, json_object, debug_mode):
        self.json_object = json_object
        self.boosting = float(0.9)
        self.unboosting = float(0.7)
        self.smalltalk_threshold = float(0.5)
        self.debug_mode = debug_mode

    def change_output(self):
        # Boost to 0.9 if the score of the highest RelatedIntents is greater than 0.3
        # Else boost all remaining RelatedIntents to 0.7
        if 'Rivescript_'.lower() in str(self.json_object.get('Intent')[0]['classification']).lower() and \
                float(self.json_object.get('RelatedIntents')[0]['prob']) > self.smalltalk_threshold:

            # Boosting to 0.9 the result of the highest intention predicted result
            self.json_object.get('Intent')[0]['classification'] = \
                self.json_object.get('RelatedIntents')[0]['classification']
            self.json_object.get('Intent')[0]['prob'] = self.boosting

        elif float(self.json_object.get('RelatedIntents')[0]['prob']) > float(0.3) and \
                'Rivescript_'.lower() not in str(self.json_object.get('Intent')[0]['classification']).lower():

            # Boosting to 0.9 the result of the highest intention predicted result
            self.json_object.get('Intent')[0]['classification'] = \
                self.json_object.get('RelatedIntents')[0]['classification']
            self.json_object.get('Intent')[0]['prob'] = self.boosting
        else:
            # Boosting to 0.7 the rest of the items items in RelatedIntents
            RelatedIntents = self.json_object.get('RelatedIntents')
            boostedIntents07 = []
            for i in range(0, len(RelatedIntents)):
                boostedIntents07.append({'classification': RelatedIntents[i]['classification'], 'prob': float(0.7)})
            self.json_object['Intent'] = boostedIntents07

        # Get Related Intents to delete the first value
        self.json_object_modified = self.json_object.get('RelatedIntents')
        # Save the first value to add it later in debug=True
        first_related = self.json_object_modified[0]
        # Delete the first value
        del self.json_object_modified[0]

        if self.debug_mode == 'True':
            # Get the original probability of Intent's result
            first_related_prob = float(first_related.get('prob'))
            self.json_object.get('Intent')[0]['original_prob'] = float(first_related_prob)
            return self.json_object
        else:
            # Get the original probability of Intent's result
            first_related_prob = float(first_related.get('prob'))
            self.json_object.get('Intent')[0]['original_prob'] = float(first_related_prob)

            # Delete Modified Query and modified querys
            self.json_object.pop('ModifiedQuery')
            self.json_object.pop('Modified Querys')
            return self.json_object

    def boost_json(self):
        json_output = self.change_output()
        return json_output

    def outputs_to_unboosting(self):
        # Unboost the intent elements
        original_prob_0 = self.json_object.get('Intent')[0]['prob']
        self.json_object.get('Intent')[0]['original_prob'] = float(original_prob_0)
        self.json_object.get('Intent')[0]['prob'] = self.unboosting

        original_prob_1 = self.json_object.get('Intent')[1]['prob']
        self.json_object.get('Intent')[1]['original_prob'] = float(original_prob_1)
        self.json_object.get('Intent')[1]['prob'] = self.unboosting

        # Delete the first value
        del self.json_object.get('RelatedIntents')[0:2]

        if self.debug_mode != 'True':
            # Delete Modified Query and modified querys
            self.json_object.pop('ModifiedQuery')
            self.json_object.pop('Modified Querys')

        return self.json_object
