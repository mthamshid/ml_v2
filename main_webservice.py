from flask import Flask, jsonify, request
from intent_classifier.Predict.predict_model import *
from intent_classifier.Utils.load_parameters import *
import os

# Run: ssh -R 80:localhost:8080 ssh.localhost.run

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
app = Flask(__name__)

params, model_config = load_parameters()
Predict_Model = PredictModel(params, model_config)
Predict_Model.load()

@app.route('/getIntent', methods=['GET'])
def get_intent():
    user_query = request.args['query']
    try:
        debug_mode = request.args['debug']

    except Exception as e:
        debug_mode = 'False'

    if debug_mode == 'True':
        json_array = Predict_Model.predict(user_query, debug_mode)
    else:
        json_array = Predict_Model.predict(user_query, debug_mode)

    return jsonify(json_array)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False, port=int(params.get('port')), use_reloader=False, threaded=False)


# """If you want to run from outside world, use the following snippet."""
# MODEL_DIR = "/home/juan/CIRA_ML_2/models/"
# PORT=8080
# LANG= 'en'
#
# params, model_config = load_parameters(model_dir=MODEL_DIR, lang=LANG, port=PORT)
#
# Predict_Model = PredictModel(params, model_config)
# Predict_Model.load()
#
# # user_query = "know more about ACH"
# # debug_mode = "False"
#
# # json_array = Predict_Model.predict(user_query, debug_mode)
#
# @app.route('/getIntent', methods=['GET'])
# def get_intent():
#     user_query = request.args['query']
#     try:
#         debug_mode = request.args['debug']
#
#     except Exception as e:
#         debug_mode = 'False'
#
#     if debug_mode == 'True':
#         json_array = Predict_Model.predict(user_query, debug_mode)
#     else:
#         json_array = Predict_Model.predict(user_query, debug_mode)
#
#     return jsonify(json_array)
#
#
# if __name__ == "__main__":
#     app.run(host='0.0.0.0', debug=False, port=int(params.get('port')), use_reloader=False, threaded=False)
